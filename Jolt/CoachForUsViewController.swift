//
//  CoachForUsViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 3/17/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class CoachForUsViewController: UIViewController {

     @IBOutlet var btn_findCoach: UIButton!
     @IBOutlet var btn_coachForUs: UIButton!
     @IBOutlet var btn_blog: UIButton!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "COACH TYPE"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        btn_findCoach.layer.cornerRadius = btn_findCoach.frame.size.height/2
        btn_coachForUs.layer.cornerRadius = btn_coachForUs.frame.size.height/2
         btn_blog.layer.cornerRadius = btn_blog.frame.size.height/2
    }
    
    
    @IBAction func btn_careerAdviceAction(_ sender: Any) {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"CareerAdviceViewController") as! CareerAdviceViewController
         self.navigationItem.title = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func btn_findRecuiter(_ sender: Any) {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"FindRecruiterViewController") as! FindRecruiterViewController
         self.navigationItem.title = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_blog(_ sender: Any)
    {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"BlogViewController") as! BlogViewController
        self.navigationItem.title = ""
        self.navigationController?.pushViewController(vc, animated: true)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
