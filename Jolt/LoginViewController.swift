//
//  LoginViewController.swift
//  Jolt
//
//  Created by ChawtechSolutions on 22/02/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn

class LoginViewController: UIViewController,GIDSignInDelegate,GIDSignInUIDelegate,UITextFieldDelegate {
    
   


     @IBOutlet weak var btn_checkBox: UIButton!
    @IBOutlet weak var txtf_password: CustomTextField!
    @IBOutlet weak var txtf_email: CustomTextField!
    var activityIndicatorView: ActivityIndicatorView!
    var socialname = String()
    var socialemail = String()
    var dict : [String : AnyObject]!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // TODO:- set textfield placeholder
        txtf_email.attributedPlaceholder = NSAttributedString(string:"Email", attributes: [NSForegroundColorAttributeName: UIColor.init(red: 121/255.0, green: 132/255.0, blue: 140/255.0, alpha: 1)])
        
        txtf_password.attributedPlaceholder = NSAttributedString(string:"Password", attributes: [NSForegroundColorAttributeName: UIColor.init(red: 121/255.0, green: 132/255.0, blue: 140/255.0, alpha: 1)])
        
        txtf_email.layer.borderColor = UIColor.lightGray.cgColor
        txtf_email.layer.borderWidth = 1
        txtf_password.layer.borderColor = UIColor.lightGray.cgColor
        txtf_password.layer.borderWidth = 1
        
         self.txtf_email.bs_setupErrorMessageView(withMessage: "Please Enter Email")
         self.txtf_password.bs_setupErrorMessageView(withMessage: "Please Enter Password")
        
    }
    
   
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        textField.bs_hideError()
    }
    @IBAction func btn_loginAction(_ sender: Any) {

         if ((txtf_email.text?.characters.count)! > 0)
         {
            if ((txtf_password.text?.characters.count)! > 0)
            {
                if isValidEmail(testStr: txtf_email.text!)  == true
            {
                if btn_checkBox.isSelected == true
                {
                    self.callLoginMethod()
                }
                else
                {
                    self.displayAlertWithMessage(viewController:self, title: "Alert!", message: "Please Accept Terms And Conditions")
                }
            }
            else
            {
                self.txtf_email.bs_setupErrorMessageView(withMessage: "Please Enter Valid  Email")
                self.txtf_email.bs_showError()
               
            }
            }
            else
            {
               
                self.txtf_password.bs_showError()
               
            }
         }
        else
         {
            self.txtf_email.bs_showError()
           
        }
        

    }
    
    
    //MARK:- btn checkbox Having tyre action
    @IBAction func btn_termsandConditions(sender: UIButton)
    {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"TermsAndConditionsViewController") as! TermsAndConditionsViewController
        self.present(vc, animated: true, completion: nil)
        //self.navigationController?.pushViewController(vc, animated: false)
    }
    
    
    //MARK:- btn checkbox Having tyre action
    @IBAction func btn_checkboxterms(sender: UIButton)
    {
        if (btn_checkBox.isSelected == true)
        {
            btn_checkBox.setBackgroundImage(UIImage(named:"unselected_checkbox"), for: UIControlState.normal)
           
            btn_checkBox.isSelected = false;
        }
        else
        {
            btn_checkBox.setBackgroundImage(UIImage(named:"selected_checkbox"), for: UIControlState.normal)
           
            btn_checkBox.isSelected = true;
        }
    }

    @IBAction func btn_createNewAccountAction(_ sender: Any) {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"RegistrationViewController") as! RegistrationViewController
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func btn_forgetPasswordAction(_ sender: Any) {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"ForgetpasswordViewController") as! ForgetpasswordViewController
        self.navigationController?.pushViewController(vc, animated: false)
    }

    
    //MARK:- check email format function
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    //MARK:-  server call of login method
    func callLoginMethod()
    {
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            
        }
        let apiCall = JsonApi()
        let parameters = [
            
            "email": txtf_email.text!,
            "password": txtf_password.text!,
            "device": "ios",
            "fcm_key": UserDefaults.standard.object(forKey: "fcm_token")!
        ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalLoginUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            let status = result["status"] as! String
            if status != ""
            {
                let success = result["success"] as! String
                let message = result["message"] as! String
                
                print(status)
                if success == "true"
                {
                    // set all key values in user defaults
                    let data = result["data"] as! NSDictionary
                    UserDefaults.standard.setValue(data["email"], forKey: "email")
                    
                    
                    UserDefaults.standard.setValue(data["name"], forKey: "name")
                    
                    UserDefaults.standard.setValue(data["id"], forKey: "id")
                    UserDefaults.standard.setValue(data["registerDate"], forKey: "registerDate")
                    UserDefaults.standard.setValue(data["password"], forKey: "password")
                    // call next view controller in main queue
                    DispatchQueue.main.async {
                        self.activityIndicatorView.stopAnimating()
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let vc  = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
                        vc.selectedIndex = 1
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                }
                else
                {
                    // call alert controller in main queue
                    DispatchQueue.main.async {
                         self.activityIndicatorView.stopAnimating()
                        self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:message)
                    }
                }
                
            }
            else
            {
                DispatchQueue.main.async {
                    self.activityIndicatorView.stopAnimating()
                    self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:"Server Error! Please try again")
                }

            }
        }
    }

    
    @IBAction func btnFBLoginPressed(_ sender: AnyObject) {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                        fbLoginManager.logOut()
                    }
                }
            }
        }
    }
    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    self.dict = result as! [String : AnyObject]
                    print(result!)
                    self.socialname =  self.dict["name"] as! String
                    self.socialemail =  self.dict["email"] as! String
                    print(self.dict)
                    self.callsocialRegisterMethod()
                }
            })
        }
    }
    // MARK:- Server call of social register Method
    func callsocialRegisterMethod()
    {
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
        }
        
        let apiCall = JsonApi()
        let parameters = [
            
            "password": "",
            "email": socialemail,
            "name": socialname,
            "registration_type":"social",
            "device": "ios",
            "fcm_key": UserDefaults.standard.object(forKey:"fcm_token")!
            
            
        ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalRegisterUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            let status = result["status"] as! String
            if status != ""
            {
                let success = result["success"] as! String
                let message = result["message"] as! String
                
                print(status)
                if success == "true"
                {
                    // set all key values in user defaults
                    let data = result["data"] as! NSDictionary
                    
                    UserDefaults.standard.setValue(data["email"], forKey: "email")
                    
                    
                    UserDefaults.standard.setValue(data["name"], forKey: "name")
                    
                    
                    UserDefaults.standard.setValue(data["id"], forKey: "id")
                    UserDefaults.standard.setValue(data["registerDate"], forKey: "registerDate")
                    UserDefaults.standard.setValue(data["password"], forKey: "password")
                    
                    // call next view controller in main queue
                    DispatchQueue.main.async {
                        self.activityIndicatorView.stopAnimating()
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let vc  = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
                        vc.selectedIndex = 1
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                    
                }
                else
                {
                    // call alert controller
                    DispatchQueue.main.async
                        {
                            self.activityIndicatorView.stopAnimating()
                            self.displayAlertWithMessage(viewController: self, title: "Alert!", message:message)
                    }
                }
                
            }
            else
            {
                DispatchQueue.main.async {
                    self.activityIndicatorView.stopAnimating()
                    self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:"Server Error! Please try again")
                }
            }
        }
    }
    
   
    @IBAction func btn_googleLoginAction(_ sender: Any)
    {
       // GIDSignIn.sharedInstance().clientID = kgoogleClientID
        GIDSignIn.sharedInstance().uiDelegate = self
         GIDSignIn.sharedInstance().delegate = self
        
        GIDSignIn.sharedInstance().signOut()
        let signIn = GIDSignIn.sharedInstance()
        signIn?.shouldFetchBasicProfile = true
        signIn?.clientID = kgoogleClientID
        signIn?.scopes = ["profile", "email"]
        signIn?.delegate = self
        signIn?.uiDelegate = self
        // self.statusField.text = @"Initialized auth2...";
        GIDSignIn.sharedInstance().signIn()
    }

    
    
    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!,
                withError error: NSError!) {
        if (error == nil) {
//            // Perform any operations on signed in user here.
//            let userId = user.userID                  // For client-side use only!
//            let idToken = user.authentication.idToken // Safe to send to the server
//            let fullName = user.profile.name
//            let givenName = user.profile.givenName
//            let familyName = user.profile.familyName
//            let email = user.profile.email
            // ...
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
//            let userId = user.userID                  // For client-side use only!
//            let idToken = user.authentication.idToken // Safe to send to the server
//            let fullName = user.profile.name
//            let givenName = user.profile.givenName
//            let familyName = user.profile.familyName
//            let email = user.profile.email
            
            socialname = user.profile.name
            socialemail = user.profile.email
            self.callsocialRegisterMethod()
            // ...
        } else {
            print("\(error.localizedDescription)")
        }

        
    }
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user:GIDGoogleUser!,
                withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
    
//    // pressed the Sign In button
//    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
//        activityIndicatorView.stopAnimating()
//    }
    
    // Present a view that prompts the user to sign in with Google
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
