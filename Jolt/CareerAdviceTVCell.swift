//
//  CareerAdviceTVCell.swift
//  Jolt
//
//  Created by chawtech solutions on 4/10/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class CareerAdviceTVCell: UITableViewCell {

    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_subtitle: UILabel!
    
    @IBOutlet weak var image_view: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
