//
//  Constant.swift
//  YouTow
//
//  Created by chawtech solutions on 1/17/17.
//  Copyright © 2017 chawtech solutions. All rights reserved.
//

import UIKit

//MARK:- google login client id
var kgoogleClientID = "258313360966-tnc401ovp7anrqkeqi330r6b76q09op4.apps.googleusercontent.com"
var dataBaseName = "Jolt.DB"

//MARK:- server url
//var kbaseUrl = "http://35.167.21.236/joltapi/"
var kbaseUrl =  "http://192.225.175.100/joltapi/"
var kadditionalLoginUrl = "appusers/login"
var kadditionalRegisterUrl = "appusers/registration"
var kadditionalgetQuestionire = "assessments/question"
var kadditionalLogoutUrl = "appusers/logout"
var kadditionalAssismentResultUrl = "assessments/rate"
var kadditionalforgetPasswordUrl = "appusers/forgetpassword"
var kadditionaltermsAndConditionsUrl = "cms/terms"
var kadditionalgetPersonalityUrl = "assessments/personalities"
var kadditionaleditProfileUrl = "appusers/editprofile"
var kadditionalgetProfileUrl = "appusers/getprofile"
var kadditionalupdateFcmTokensUrl = "appusers/updatefcm"
var kadditionalcoachRegisterUrl = "appusers/coach_register"
var kadditionalcoachListUrl = "appusers/coachlist"
var kadditionalcoachCategoryUrl = "appusers/coach_category"
var kadditionalminibusinessPlanUrl = "businessplans/addplan"
var kadditionalrecruiterListUrl = "recruiters/recruitlist"
var kadditionalGetbusinesschecklistUrl = "businessplans/allchecklist"
var kadditionalAddbusinesschecklistUrl = "businessplans/addchecklist"
var kadditionalGetCareerchecklistUrl = "career/allchecklist"
var kadditionalAddCareerchecklistUrl = "career/addchecklist"
var kadditionaljobBoardlistUrl = "https://api-v2.themuse.com/jobs?page="
var kadditionaladdcareerPlanUrl = "career/addplan"
var kadditionalupdatePaymentDetailUrl = "planpayment/adddetail"
var kadditionalgetPaymentstatusUrl = "planpayment/getstatus"
var kadditionalfeedbackUrl = "feedback/form"
var kadditionalsetCareerReminderUrl = "appusers/updatetime"
extension UIViewController {
    
    //MARK:- set alert controller globally
    func displayAlertWithMessage(viewController: UIViewController, title: String, message: String){
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title:"OK", style: .default, handler: nil))
        viewController.present(ac, animated: true){}
    }
}

extension String {
    var html2AttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
class Constant: NSObject {

    
    
}
