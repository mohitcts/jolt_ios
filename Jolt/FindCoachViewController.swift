//
//  FindCoachViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 3/20/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class FindCoachViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate,UITextViewDelegate {
   

  var activityIndicatorView: ActivityIndicatorView!
     @IBOutlet var btn_submit: UIButton!
     @IBOutlet var txtf_abouturself: UITextView!
      @IBOutlet var txtf_fname: CoachTextField!
      @IBOutlet var txtf_lname: CoachTextField!
      @IBOutlet var txtf_email: CoachTextField!
      @IBOutlet var txtf_mobile: CoachTextField!
      @IBOutlet var txtf_category: CoachTextField!
      @IBOutlet var txtf_exp: CoachTextField!
     let pickerDataArr =  NSMutableArray()// ["Business","Category"]
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "REGISTRATION"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewDidLoad() {
        super.viewDidLoad()
      
       
        txtf_category.delegate = self
        txtf_fname.layer.borderWidth = 1
        txtf_fname.layer.borderColor = UIColor.lightGray.cgColor
        txtf_lname.layer.borderWidth = 1
        txtf_lname.layer.borderColor = UIColor.lightGray.cgColor
        txtf_email.layer.borderWidth = 1
        txtf_email.layer.borderColor = UIColor.lightGray.cgColor
        txtf_mobile.layer.borderWidth = 1
        txtf_mobile.layer.borderColor = UIColor.lightGray.cgColor
        txtf_abouturself.layer.borderWidth = 1
        txtf_abouturself.layer.borderColor = UIColor.lightGray.cgColor
        txtf_category.layer.borderWidth = 1
        txtf_category.layer.borderColor = UIColor.lightGray.cgColor
        txtf_exp.layer.borderWidth = 1
        txtf_exp.layer.borderColor = UIColor.lightGray.cgColor
        txtf_abouturself.delegate = self
        btn_submit.layer.cornerRadius = btn_submit.frame.size.height/2
        
        self.txtf_fname.bs_setupErrorMessageView(withMessage: "Please Enter First Name")
        self.txtf_lname.bs_setupErrorMessageView(withMessage: "Please Enter Last Name")
        self.txtf_email.bs_setupErrorMessageView(withMessage: "Please Enter Email")
        self.txtf_mobile.bs_setupErrorMessageView(withMessage: "Please Enter Mobile")
        self.txtf_exp.bs_setupErrorMessageView(withMessage: "Please Enter Experience")
       

        
       self.callgetCategoryListMethod()


    }
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if  txtf_abouturself.text == "About Yourself" {
            txtf_abouturself.text = nil
            txtf_abouturself.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if  txtf_abouturself.text == ""{
            txtf_abouturself.text = "About Yourself"
            txtf_abouturself.textColor = UIColor.lightGray
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.createPickerview()
        textField.bs_hideError()
    }
    
    
func createPickerview()
{
    var picker = UIPickerView()
    picker = UIPickerView(frame: CGRect(x:0,y: 200, width:view.frame.width,height:200))
    picker.backgroundColor = .white
    
    picker.showsSelectionIndicator = true
    picker.delegate = self
    picker.dataSource = self
    
    let toolBar = UIToolbar()
    toolBar.barStyle = UIBarStyle.default
    toolBar.isTranslucent = true
    toolBar.tintColor = UIColor.black//UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
    toolBar.sizeToFit()
    
    let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(FindCoachViewController.donePicker))
    let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
    let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(FindCoachViewController.donePicker))
    
    toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
    toolBar.isUserInteractionEnabled = true
    
    txtf_category.inputView = picker
    txtf_category.inputAccessoryView = toolBar
    }
    
   
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataArr.count
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
       // return pickerDataArr[row]
        let dict = pickerDataArr[row] as? Dictionary<String,String>
        return(dict?["category_name"])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
       // txtf_category.text = pickerDataArr[row]
       // txtf_category.text = pickerDataArr[row]["category_name"]
         let dict = pickerDataArr[row] as? Dictionary<String,String>
        txtf_category.text = dict?["category_name"]
    }
    
    func donePicker() {
        
        txtf_category.resignFirstResponder()
        
    }
    // MARK:- Server call of getService Provider Method
    func callgetCategoryListMethod()
    {
        
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            // UIApplication.shared.beginIgnoringInteractionEvents()
        }
        let string_url = kbaseUrl + kadditionalcoachCategoryUrl
        
        let url = URL(string: string_url)
        
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            // let json = try! JSONSerialization.jsonObject(with: data, options: [])
            let json = try! JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject]
            
            print(json as Any)
            // parse data from server
            let categoryArr = json?["data"]  as? [[String: Any]]
            if categoryArr?.count != 0
            {
                self.parseGetCareerListData(dataArray: categoryArr!)
            }
            else
            {
                DispatchQueue.main.async {
                    self.activityIndicatorView.stopAnimating()
                    // UIApplication.shared.beginIgnoringInteractionEvents()
                }
            }
        }
        
        task.resume()
        //        let apiCall = JsonApi()
        //        let parameters = [
        //
        //
        //
        //            ]
        //
        //        apiCall.callUrlSession(urlValue: "https://api-v2.themuse.com/posts?page=0", para: parameters as (AnyObject), isSuccess: true)
        //        { (result) -> Void in
        //
        //            print("service-",result)
        //
        //            let success = result["success"] as! String
        //            let message = result["message"] as! String
        //
        //
        //            if success == "true"
        //            {
        //                // parse data from server
        //                let venderArr=result["data"] as? [[String: Any]]
        //                self.parseGetserviceProviderListData(dataArray: venderArr!)
        //            }
        //            else
        //            {
        //                DispatchQueue.main.async
        //                    {
        //                        self.activityIndicatorView.stopAnimating()
        //
        //                        self.displayAlertWithMessage(viewController:self,title:"Alert!" ,message:message)
        //
        //                }
        //
        //            }
        //        }
    }
    
    
    //  MARK:- parseGetserviceProviderListData
    func parseGetCareerListData(dataArray: [[String: Any]])
    {
    
        let categoryDict = NSMutableDictionary()
      
        for i in 0...dataArray.count-1
        {
            
            let Object = dataArray[i] as [String : AnyObject]
            
            categoryDict.setValue((Object["category_name"]), forKey: "category_name")
            categoryDict.setValue((Object["catid"] as? NSString), forKey: "catid")
            pickerDataArr.add(categoryDict .mutableCopy())
            
            
        }
        DispatchQueue.main.async
            {
               self.txtf_category.text =  dataArray[0]["category_name"] as? String
                self.activityIndicatorView.stopAnimating()
                
        }
    }
  
    
    //MARK:- btn signup Action
    @IBAction func btn_submitAction(_ sender: Any)
    {
        
        if ((txtf_fname.text?.characters.count)! > 0)
        {
            if ((txtf_lname.text?.characters.count)! > 0)
            {
            if ((txtf_email.text?.characters.count)! > 0)
            {
                if  isValidEmail(testStr:txtf_email.text!)  == true
                {
                    if ((txtf_mobile.text?.characters.count)! > 0)
                    {
                        if ((txtf_exp.text?.characters.count)! > 0)
                        {
                            
                            self.callCoachRegisterMethod()
                            
                        }
                        else
                        {
                            self.txtf_exp.bs_showError()
                        }
                    }
                    else
                    {
                        self.txtf_mobile.bs_showError()
                    }
                }
                else
                {
                    self.txtf_email.bs_setupErrorMessageView(withMessage: "Please Enter Valid  Email")
                    self.txtf_email.bs_showError()
                }
            }
            else
            {
                self.txtf_email.bs_showError()
            }
            }else
            {
                self.txtf_lname.bs_showError()
            }
        }else
        {
            self.txtf_fname.bs_showError()
        }
        
        
        
        
          }
    
    //MARK:- check email format function
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    // MARK:- Server call of register Method
    func callCoachRegisterMethod()
    {
        if txtf_abouturself.text == nil
        {
            txtf_abouturself.text = ""
        }
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
        }
        
        let apiCall = JsonApi()
        let parameters = [
            
            "first_name": txtf_fname.text!,
            "last_name": txtf_lname.text!,
            "mobile": txtf_mobile.text!,
            "email": txtf_email.text!,
            "category": txtf_category.text!,
            "experience":txtf_exp.text!,
            "user_id": UserDefaults.standard.object(forKey: "id"),
            "about_your_self": txtf_abouturself.text!//UserDefaults.standard.object(forKey: "fcm_token")
            
            
        ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalcoachRegisterUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            let status = result["status"] as! String
            if status != ""
            {
                let success = result["success"] as! String
                let message = result["message"] as! String
                
                print(status)
                if success == "true"
                {
                    // call alert controller
                    DispatchQueue.main.async
                        {
                            self.activityIndicatorView.stopAnimating()
                            self.displayAlertWithMessage(viewController: self, title: "Alert!", message:message)
                    }
                }
                else
                {
                    // call alert controller
                    DispatchQueue.main.async
                        {
                            self.activityIndicatorView.stopAnimating()
                            self.displayAlertWithMessage(viewController: self, title: "Alert!", message:message)
                    }
                }
                
            }
            else
            {
                // call alert controller
                DispatchQueue.main.async
                    {
                        self.activityIndicatorView.stopAnimating()
                        self.displayAlertWithMessage(viewController: self, title: "Alert!", message:"Server Error! Please try again")
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
