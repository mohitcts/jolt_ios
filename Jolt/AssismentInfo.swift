//
//  AssismentInfo.swift
//  Jolt
//
//  Created by chawtech solutions on 3/21/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class AssismentInfo: NSObject {
    
    var questionid: String = String()
    var firstquestion: String = String()
     var secondquestion: String = String()
    var category: String = String()
     var answer: String = String()

}
