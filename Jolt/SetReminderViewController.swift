//
//  SetReminderViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 5/23/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class SetReminderViewController: UIViewController ,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate {
    
    
    var activityIndicatorView: ActivityIndicatorView!
    @IBOutlet var btn_set: UIButton!
 
    @IBOutlet var txtf_reminder: UITextField!
   var pickerDataArr = [String]()//["15","30","45","60","75","90"]


    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = "SET REMINDERS"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        txtf_reminder.delegate = self
         txtf_reminder.attributedPlaceholder = NSAttributedString(string:"Number of Days", attributes: [NSForegroundColorAttributeName: UIColor.gray])
          txtf_reminder.layer.sublayerTransform = CATransform3DMakeTranslation(10,0,0)
        btn_set.layer.cornerRadius = btn_set.frame.size.height/2
        for i in 1...30
        {
          pickerDataArr.append(String(i))
            
        }
        
        
    }
 
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.createPickerview()
        
    }
    
    @IBAction func btn_setAction(_ sender: Any)
    {
        if (txtf_reminder.text?.characters.count)! > 0 {
            
            self.callsetReminderMethod()
        }
    }
    //MARK:-  server call of login method
    func callsetReminderMethod()
    {
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            
        }
        let apiCall = JsonApi()
        let parameters = [
            
            "days": txtf_reminder.text!,
            "user_id": UserDefaults.standard.object(forKey: "id")!
        ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalsetCareerReminderUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            let status = result["status"] as! String
            if status != ""
            {
                let success = result["success"] as! String
               // let message = result["message"] as! String
                
                print(status)
                if success == "true"
                {
                    DispatchQueue.main.async {
                        self.activityIndicatorView.stopAnimating()
                    // set all key values in user defaults
                    let actionSheetController: UIAlertController = UIAlertController(title: "Message", message: "You are successfully added Days.", preferredStyle: .alert)
                    let okAction: UIAlertAction = UIAlertAction(title: "OK", style: .default) { action -> Void in
                        //Just dismiss the action sheet
                        self.navigationController?.popViewController(animated: true)
                    }
                    actionSheetController.addAction(okAction)
                    self.present(actionSheetController, animated: true, completion: nil)
                    }
                }
                else
                {
                    // call alert controller in main queue
                    DispatchQueue.main.async {
                        self.activityIndicatorView.stopAnimating()
                        self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:"Some error happen! Please try again")
                    }
                }
                
            }
            else
            {
                DispatchQueue.main.async {
                    self.activityIndicatorView.stopAnimating()
                    self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:"Server Error! Please try again")
                }
                
            }
        }
    }
    

    func createPickerview()
    {
        var picker = UIPickerView()
        picker = UIPickerView(frame: CGRect(x:0,y: 200, width:view.frame.width,height:200))
        picker.backgroundColor = .white
        
        picker.showsSelectionIndicator = true
        picker.delegate = self
        picker.dataSource = self
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black//UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(SetReminderViewController.donePicker))
       // let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
      //  let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(SetReminderViewController.donePicker))
        
        toolBar.setItems([doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        txtf_reminder.inputView = picker
        txtf_reminder.inputAccessoryView = toolBar
    }
    
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataArr.count
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
         return pickerDataArr[row]
        //let dict = pickerDataArr[row] as? Dictionary<String,String>
        //return(dict?["category_name"])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
         txtf_reminder.text = pickerDataArr[row]
        // txtf_category.text = pickerDataArr[row]["category_name"]
        //let dict = pickerDataArr[row] as? Dictionary<String,String>
        //txtf_reminder.text = dict?["category_name"]
    }
    
    func donePicker() {
        
        txtf_reminder.resignFirstResponder()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
