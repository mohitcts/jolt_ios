//
//  DashboardCVCell.swift
//  Jolt
//
//  Created by chawtech solutions on 4/4/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class DashboardCVCell: UICollectionViewCell {
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var image_view: UIImageView!
}
