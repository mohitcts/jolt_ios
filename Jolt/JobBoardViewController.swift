//
//  JobBoardViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 3/17/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class JobBoardViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{

    var activityIndicatorView: ActivityIndicatorView!
    @IBOutlet weak var tbl_jobBoard: UITableView!
    @IBOutlet weak var txtf_search: UITextField!
    var  jobBoardArr = NSMutableArray()
    var  filteredArr = NSMutableArray()
    var  paging = Int()
    var filterStringUrl = String()
    var  isMoreDataAvailable : Bool = false
    var  isfilterDataAvailable : Bool = false
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "JOB BOARD"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
      
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
      
       // NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "jobfilterprovider"), object: nil)
       
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        txtf_search.layer.sublayerTransform = CATransform3DMakeTranslation(20, 2, 2)

        txtf_search.layer.cornerRadius = 5
        txtf_search.layer.borderColor = UIColor.lightGray.cgColor
        txtf_search.attributedPlaceholder = NSAttributedString(string:"Search...", attributes: [NSForegroundColorAttributeName: UIColor.darkGray])
        paging = 0
        self.callgetJobListMethod(kbaseurl:kadditionaljobBoardlistUrl)
        tbl_jobBoard.tableHeaderView = UIView(frame: CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: CGFloat(0.0), height: CGFloat(20)))
         self.txtf_search.delegate = self
        
        // Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector(self.jobsearchFilter(_:)), name: NSNotification.Name(rawValue: "jobfilterprovider"), object: nil)
       
    }
    
    // handle notification
    func jobsearchFilter(_ notification: NSNotification) {
        
        jobBoardArr.removeAllObjects()
        jobBoardArr = (notification.userInfo?["jobarray"] as? NSMutableArray)!
        let urlInfo = (notification.userInfo?["url"] as? NSMutableArray)!
        filterStringUrl = urlInfo.object(at:0) as! String
        tbl_jobBoard.reloadData()
        isfilterDataAvailable = true
        paging = 0
    
    }
//    func filterContentForSearchText(searchText: String) {
//        jobBoardArr = jobBoardArr .filter { term in
//            return term.lowercased().contains(searchText.lowercased())
//        }
//    }
    
//    let animalArray: NSMutableArray = ["Dog","Cat","Otter","Deer","Rabbit"]
//    let filteredAnimals = animalArray.filter { $0.contains("er") }
//    print("filteredAnimals:", filteredAnimals)
//    
//    filteredAnimals: [Otter, Deer]
    

    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtf_search.resignFirstResponder()
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"SearchViewController") as! SearchViewController
        self.navigationItem.title = ""
       
        self.navigationController?.pushViewController(vc, animated: true)
    }
   
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        txtf_search.delegate = nil
//        txtf_search.resignFirstResponder()
//    }
    
    
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
//    {
//        // text hasn't changed yet, you have to compute the text AFTER the edit yourself
//        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
//        
//
//       
//        
//        if updatedString == ""
//        {
//            txtf_search.text = nil
//            txtf_search.resignFirstResponder()
//        }
//        
//        // Put your key in predicate that is "Name"
//        let searchPredicate = NSPredicate(format: "company_name CONTAINS[C] %@", updatedString!)
//        let array = (jobBoardArr as NSArray).filtered(using: searchPredicate)
//        
//        print ("array = \(array)")
//        
//        if(array.count == 0){
//           // searchActive = false;
//            jobBoardArr.add(filteredArr.mutableCopy())
//            
//            print("false")
//        } else {
//           // searchActive = true;
//          
//            jobBoardArr.removeAllObjects()
//            jobBoardArr = NSMutableArray(array: array)
//
//             print("true")
//        }
//        
//        self.tbl_jobBoard.reloadData()
//        // always return true so that changes propagate
//        return true
//    }
//

    
    // MARK:- Server call of getService Provider Method
    func callgetJobListMethod(kbaseurl:String)
    {
        
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            // UIApplication.shared.beginIgnoringInteractionEvents()
        }
        let string_url = kbaseurl + String(paging)
       
      //  let url = URL(string: string_url)
         let url = URL(string: string_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            // let json = try! JSONSerialization.jsonObject(with: data, options: [])
            let json = try! JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject]
            
            print(json as Any)
            // parse data from server
            let careerArr = json?["results"] as! NSArray
            if careerArr.count != 0
            {
                self.parseGetCareerListData(dataArray: careerArr)
                self.isMoreDataAvailable = true
            }
            else
            {
                DispatchQueue.main.async {
                    self.isMoreDataAvailable = false
                    self.activityIndicatorView.stopAnimating()
                    // UIApplication.shared.beginIgnoringInteractionEvents()
                }
            }
        }
        
        task.resume()
    }
    
    
    //  MARK:- parseGetserviceProviderListData
    func parseGetCareerListData(dataArray:NSArray)
    {
    
        let careerDict = NSMutableDictionary()
        let arrJson = dataArray as! Array<Dictionary<String,Any>>
        for i in 0...dataArray.count-1
        {
            
            let Object = arrJson[i] as [String : AnyObject]
            
            careerDict.setValue((Object["name"]), forKey: "name")
           // careerDict.setValue((Object["contents"] as? NSString), forKey: "contents")
            let company = Object["company"] as! NSDictionary
             let locations = Object["locations"] as! NSArray
            if locations.count != 0
            {
            let obj = locations[0] as! [String : AnyObject]
            careerDict.setValue((obj["name"] as? NSString), forKey: "location_name")
            }
            else
            {
                 careerDict.setValue("Location Not Available", forKey: "location_name")
            }
            let refs = Object["refs"] as! NSDictionary
            
            
            careerDict.setValue((company["name"] as? NSString), forKey: "company_name")
            careerDict.setValue((refs["landing_page"] as? NSString), forKey: "landing_page")
            careerDict.setValue((refs["primary_image"] as? NSString), forKey: "primary_image")
            jobBoardArr.add(careerDict .mutableCopy())
           
            
        }
        
        DispatchQueue.main.async
            {
                self.activityIndicatorView.stopAnimating()
                self.tbl_jobBoard.reloadData()
            }
    }
    
    //MARK:- tableview delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return jobBoardArr.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(
            withIdentifier: "cell",
            for: indexPath)as! JobBoardTVCell
        
        
        let dict = jobBoardArr[indexPath.section] as? Dictionary<String,String>
        //  cell.backgroundColor = UIColor.white
        
        cell.lbl_title.text = dict?["company_name"]
        let location_name =  (dict?["location_name"])!as String
        let name =  (dict?["name"])!as String
        let full_name = location_name + "\n" + name
        
         cell.lbl_subtitle.text = full_name
       // cell.lbl_subtitle.text = dict?["contents"]
        // cell.image_view.image = UIImage.init(named: (dict?["primary_image"])!)
      //  let imageString =  dict?["primary_image"]
        
        
//        
//        DispatchQueue.main.async {
//            let imgURL = URL(string:imageString!)
//            cell.image_view.kf.setImage(with: imgURL)}
        
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
       
             return 2
        
       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 90
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let dict = jobBoardArr[indexPath.section] as? Dictionary<String,String>
        let url = (dict?["landing_page"])! as String
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"JobBoardDetailViewController") as! JobBoardDetailViewController
        self.navigationItem.title = ""
        vc.detail_url = url
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if(indexPath.section == (self.jobBoardArr.count-1)) {
            if isMoreDataAvailable == true {
            paging = paging + 1
                if isfilterDataAvailable == true
                {
                      self.callgetJobListMethod(kbaseurl: filterStringUrl)
                }
                else
                {
                      self.callgetJobListMethod(kbaseurl: kadditionaljobBoardlistUrl)
                }
         
            }
            
            
        }
        
        
    }
    // MARK:- Server call of getService Provider Method
    func callgetJobFilterListMethod()
    {
        
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            // UIApplication.shared.beginIgnoringInteractionEvents()
        }
        
        //  https://api-v2.themuse.com/jobs?search=Fremantle&category=Account+Management&location=Accra&page=1
        
        
        
        
        // let url = URL(string:string_url)
        let url = URL(string: filterStringUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            // let json = try! JSONSerialization.jsonObject(with: data, options: [])
            let json = try! JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject]
            
            print(json as Any)
            // parse data from server
            let careerArr = json?["results"] as! NSArray
            if careerArr.count != 0
            {
                self.parseGetfilterJobsListData(dataArray: careerArr)
            }
            else
            {
                DispatchQueue.main.async {
                    self.activityIndicatorView.stopAnimating()
                    self.displayAlertWithMessage(viewController: self, title: "Alert!", message: "No Result Found")
                }
            }
        }
        
        task.resume()
    }
    
    
    //  MARK:- parseGetserviceProviderListData
    func parseGetfilterJobsListData(dataArray:NSArray)
    {
        let careerDict = NSMutableDictionary()
        let arrJson = dataArray as! Array<Dictionary<String,Any>>
        for i in 0...dataArray.count-1
        {
            
            let Object = arrJson[i] as [String : AnyObject]
            
            careerDict.setValue((Object["name"]), forKey: "name")
            // careerDict.setValue((Object["contents"] as? NSString), forKey: "contents")
            let company = Object["company"] as! NSDictionary
            let locations = Object["locations"] as! NSArray
            
            if locations.count != 0
            {
                let obj = locations[0] as! [String : AnyObject]
                careerDict.setValue((obj["name"] as? NSString), forKey: "location_name")
            }
            else
            {
                careerDict.setValue("Location Not Available", forKey: "location_name")
            }
            let refs = Object["refs"] as! NSDictionary
            
            
            careerDict.setValue((company["name"] as? NSString), forKey: "company_name")
            careerDict.setValue((refs["landing_page"] as? NSString), forKey: "landing_page")
            careerDict.setValue((refs["primary_image"] as? NSString), forKey: "primary_image")
            jobBoardArr.add(careerDict .mutableCopy())
            
            
        }
        DispatchQueue.main.async
            {
                self.activityIndicatorView.stopAnimating()
                self.tbl_jobBoard.reloadData()
              
                
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
