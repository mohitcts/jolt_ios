//
//  BusinessCheckListViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 4/10/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class BusinessCheckListViewController: UIViewController ,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet var btn_next: UIButton!
    @IBOutlet weak var tbl_checkList: UITableView!
    var isSelected = Bool()
    var activityIndicatorView: ActivityIndicatorView!
    var  checklistArr = NSMutableArray()
      var  updatelistArr = NSMutableArray()
  //  let checklistArr = ["My idea or business goal has been evaluated,","I know how to make money from it","I have a business plan","I have a core value statment","My idea a mission statement","I can describe  my idea or business is one statement","I have determined the target market","I can name my competitors","I have a unique but applicable name for my business","I can describe the value my idea or business proposes","I have conducted a market analysis","I know how to price my idea, products or services","I have registered my company","I need and have secured a patent, trademark or copyright","I have a plan for bookkeeping","I have necessary insurance","I have a good tracking system for my expenses","I know my exact income","I know exactly how much my business, idea needs","I have identified and identified an investor"]
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = "BUSINESS CHECKLIST"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isSelected = false
        btn_next.layer.cornerRadius = btn_next.frame.size.height/2
        self.callgetBusinessChecklistMethod()
           self.tbl_checkList.allowsMultipleSelection = true
    }
    
    
    // MARK:- Server call of getService Provider Method
    func callgetBusinessChecklistMethod()
    {
        
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            // UIApplication.shared.beginIgnoringInteractionEvents()
        }
        let apiCall = JsonApi()
        let parameters = [
            
            "user_id": UserDefaults.standard.object(forKey: "id"),
            
            ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalGetbusinesschecklistUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            
            let success = result["success"] as! String
         //   let message = result["message"] as! String
            
            
            if success == "true"
            {
                // parse data from server
                let Arr = result["data"] as? [[String: Any]]
                if Arr?.count != 0
                {
                   self.parseGetCareerListData(dataArray: Arr!)
                }
                else
                {
                    self.activityIndicatorView.stopAnimating()
                    self.displayAlertWithMessage(viewController:self,title:"Alert!" ,message:"No Data Found!")
                }
            }
            else
            {
                DispatchQueue.main.async
                {
                        self.activityIndicatorView.stopAnimating()
                self.displayAlertWithMessage(viewController:self,title:"Alert!" ,message:"")
                        
                }
                
            }
        }
    }
    
    
    //  MARK:- parseGetserviceProviderListData
    func parseGetCareerListData(dataArray:[[String: Any]])
    {
        checklistArr.removeAllObjects()
        let checkDict = NSMutableDictionary()

        for i in 0...dataArray.count-1
        {
            
            checkDict.setValue((dataArray[i]["checklist_id"] as! String), forKey: "checklist_id")
            checkDict.setValue((dataArray[i]["checklist_name"]as! String), forKey: "checklist_name")
            checkDict.setValue((dataArray[i]["check_status"]as! NSNumber), forKey: "check_status")
            checklistArr.add(checkDict .mutableCopy())
           
            
        }
        DispatchQueue.main.async
            {
                self.activityIndicatorView.stopAnimating()
                self.tbl_checkList.reloadData()
                
        }
    }

    //MARK:- tableview delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return checklistArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(
            withIdentifier: "cell",
            for: indexPath)as! BusinessCheckListTVCell
        
        
        let dict = checklistArr[indexPath.row] as? Dictionary<String,Any>
    
        cell.lbl_name.text = dict?["checklist_name"] as? String
        
        let check_status = dict?["check_status"] as? NSNumber
        if check_status == 0
        {
           // cell.btn_checkbox.setImage(UIImage.init(named:"unselected_checkbox"), for: .normal)
            cell.imgv_checkbox.image = UIImage.init(named:"unselected_checkbox")
        }
        else
        {
           // cell.btn_checkbox.setImage(UIImage.init(named:"selected_checkbox"), for: .normal)
            cell.imgv_checkbox.image = UIImage.init(named:"selected_checkbox")
            updatelistArr.add(dict?["checklist_id"]as! String)
        }
        
        
       // cell.btn_checkbox.tag = indexPath.row
       //cell.btn_checkbox.addTarget(self,action:#selector(buttonShowMeClicked(sender:)), for: .touchUpInside)
       // cell.btn_checkbox.isExclusiveTouch = true
       // cell.btn_checkbox.isSelected = false
        
        return cell
        
    }
    
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
       //let cell = tableView.cellForRow(at: indexPath as IndexPath)
       let cell =  tableView.cellForRow(at: indexPath) as! BusinessCheckListTVCell!
         let dict = checklistArr[(indexPath.row)] as? Dictionary<String,Any>
//        if cell!.isSelected == true
//        {
            //cell!.accessoryType = UITableViewCellAccessoryType.Checkmark
            cell?.imgv_checkbox.image = UIImage.init(named: "selected_checkbox")
             updatelistArr.add(dict?["checklist_id"]as! String)
//        }
//        else
//        {
//           // cell!.accessoryType = UITableViewCellAccessoryType.None
//            cell!.imgv_checkbox.image = UIImage.init(named: "unselected_checkbox")
//            let checklist_id = dict?["checklist_id"]as! String
//            let selectedArr : NSMutableArray = updatelistArr.mutableCopy() as! NSMutableArray
//            for i in 0...selectedArr.count-1
//            {
//                let arrVal = selectedArr[i]
//                if checklist_id == String(describing: arrVal)
//                {
//                    updatelistArr.removeObject(at: i)
//                }
//            }
//            
//
//        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
         let cell =  tableView.cellForRow(at: indexPath) as! BusinessCheckListTVCell!
          let dict = checklistArr[(indexPath.row)] as? Dictionary<String,Any>
//        if cell!.isSelected == true
//        {
//            //cell!.accessoryType = UITableViewCellAccessoryType.Checkmark
//             cell?.imgv_checkbox.image = UIImage.init(named: "selected_checkbox")
//        }
//        else
//        {
           // cell!.accessoryType = UITableViewCellAccessoryType.None
            cell?.imgv_checkbox.image = UIImage.init(named: "unselected_checkbox")
            let checklist_id = dict?["checklist_id"]as! String
            let selectedArr : NSMutableArray = updatelistArr.mutableCopy() as! NSMutableArray
            for i in 0...selectedArr.count-1
            {
                let arrVal = selectedArr[i]
                if checklist_id == String(describing: arrVal)
                {
                    updatelistArr.removeObject(at: i)
                }
            }

        //}
    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let cell = tbl_checkList.cellForRow(at: indexPath) as? BusinessCheckListTVCell
//        
//        //  cell?.btn_checkbox.isSelected = !cell!.btn_checkbox.isSelected
//        let dict = checklistArr[(indexPath.row)] as? Dictionary<String,Any>
//        
//        if (cell?.btn_checkbox.isSelected==true)
//        {
//            cell?.btn_checkbox.setImage(UIImage.init(named:"unselected_checkbox"), for: .normal)
//            
//            cell?.btn_checkbox.isSelected = false
//            
//            let checklist_id = dict?["checklist_id"]as! String
//            let selectedArr : NSMutableArray = updatelistArr.mutableCopy() as! NSMutableArray
//            for i in 0...selectedArr.count-1
//            {
//                let arrVal = selectedArr[i]
//                if checklist_id == String(describing: arrVal)
//                {
//                    updatelistArr.removeObject(at: i)
//                }
//            }
//        }
//            
//        else
//        {
//            
//            cell?.btn_checkbox.setImage(UIImage.init(named:"selected_checkbox"), for: .normal)
//            cell?.btn_checkbox.isSelected = true
//            updatelistArr.add(dict?["checklist_id"]as! String)
//        }
//
//    }
    
//    func buttonShowMeClicked(sender:UIButton) {
//        
//        let buttonPosition = sender.convert(CGPoint.zero, to: tbl_checkList)
//        let indexPath = tbl_checkList.indexPathForRow(at: buttonPosition)
//        
//        let cell = tbl_checkList.cellForRow(at: indexPath!) as? BusinessCheckListTVCell
//        
//        //  cell?.btn_checkbox.isSelected = !cell!.btn_checkbox.isSelected
//        let dict = checklistArr[(indexPath?.row)!] as? Dictionary<String,Any>
//        
//        if (cell?.btn_checkbox.isSelected==true)
//        {
//            cell?.btn_checkbox.setImage(UIImage.init(named:"unselected_checkbox"), for: .normal)
//            
//            cell?.btn_checkbox.isSelected = false
//         
//            let checklist_id = dict?["checklist_id"]as! String
//            let selectedArr : NSMutableArray = updatelistArr.mutableCopy() as! NSMutableArray
//            for i in 0...selectedArr.count-1
//            {
//                let arrVal = selectedArr[i]
//                if checklist_id == String(describing: arrVal)
//                {
//                    updatelistArr.removeObject(at: i)
//                }
//            }
//        }
//            
//        else
//        {
//            
//            cell?.btn_checkbox.setImage(UIImage.init(named:"selected_checkbox"), for: .normal)
//            cell?.btn_checkbox.isSelected = true
//            updatelistArr.add(dict?["checklist_id"]as! String)
//        }
//        
//        //tbl_checkList.reloadRows(at: [indexPath], with: .none)
//    }
//    
    
        
    @IBAction func btn_doneAction(_ sender: Any) {
        
        
        var jsonObject:[String:String] = [:]
      
        for i in 0...updatelistArr.count-1
        {
            jsonObject[String(i)] = updatelistArr[i] as? String
        
        }
        
        print(jsonObject)
        self.calladdBusinessChecklistMethod(jsonObject:jsonObject)
        
    }
    
    // MARK:- Server call of getService Provider Method
    func calladdBusinessChecklistMethod(jsonObject:[String:String])
    {
        
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            // UIApplication.shared.beginIgnoringInteractionEvents()
        }
        let apiCall = JsonApi()
        let parameters = [
            
            "user_id": UserDefaults.standard.object(forKey: "id")!,
            "checklist_id":jsonObject
            
            
            ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalAddbusinesschecklistUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            
            let success = result["success"] as! String
            //   let message = result["message"] as! String
            
            
            if success == "true"
            {
                
                DispatchQueue.main.async
                {
                    self.activityIndicatorView.stopAnimating()
                    _ = self.navigationController?.popViewController(animated: true)
                }
            }
            else
            {
                DispatchQueue.main.async
                {
                    self.activityIndicatorView.stopAnimating()
                    self.displayAlertWithMessage(viewController:self,title:"Alert!" ,message:"")
                        
                }
                
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
