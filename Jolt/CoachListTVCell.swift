//
//  CoachListTVCell.swift
//  Jolt
//
//  Created by chawtech solutions on 4/5/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class CoachListTVCell: UITableViewCell {

    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var lbl_category: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        for i in 1...5
        {
            
        let imageView = UIImageView(frame: CGRect(x:i*20, y: 85, width: 15, height: 15))
        imageView.image = UIImage.init(named: "nonfill_star")
       // button.setTitle("Test Button", forState: .Normal)
      //  button.addTarget(self, action: #selector(buttonAction), forControlEvents: .TouchUpInside)
        
        self.addSubview(imageView)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
