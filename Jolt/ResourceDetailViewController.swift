//
//  ResourceDetailViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 4/25/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit


class ResourceDetailViewController: UIViewController ,UIWebViewDelegate,PayPalPaymentDelegate,UIDocumentPickerDelegate  {
    
      var activityIndicatorView: ActivityIndicatorView!
    @IBOutlet weak var webview: UIWebView!
    var resourceTitle = String()
    var resourceName = String()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = resourceTitle
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
        
        //MARK: Paypal setting
        PayPalMobile.preconnect(withEnvironment: environment)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let share_btn = UIButton(type: .custom)
        share_btn.setImage(UIImage(named: "share"), for: .normal)
        //share_btn.setTitle("BUY", for: .normal)
        share_btn.setTitleColor(UIColor.init(red: 0/255.0, green: 122/255.0, blue: 255/255.0, alpha: 1), for: .normal)
        share_btn.frame = CGRect(x: 0, y: 0, width: 50, height: 30)
        share_btn.addTarget(self, action: #selector(ResourceDetailViewController.clickOnshareMethod), for: .touchUpInside)
        let share_btnn = UIBarButtonItem(customView: share_btn)
        
        self.navigationItem.setRightBarButtonItems([share_btnn], animated: true)

        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            
        }
        webview.delegate = self
        if let pdf = Bundle.main.url(forResource: resourceName, withExtension: "pdf", subdirectory: nil, localization: nil)  {
           let req = NSURLRequest(url: pdf)
            webview.loadRequest(req as URLRequest)
            self.view.addSubview(webview)
     }
        
        //MARK: paypal Intergation inislization
         self.paypalIntegration()
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        DispatchQueue.main.async {
            self.activityIndicatorView.stopAnimating()
            
        }
    }

    func paypalIntegration()
    {
        
        // Do any additional setup after loading the view.
        title = "PayPal SDK Demo"
        //successView.hidden = true
        
        // Set up payPalConfig
        print("this is credit card status \(acceptCreditCards)")
        payPalConfig.acceptCreditCards = true;
        payPalConfig.merchantName = "Awesome Shirts, Inc."
        payPalConfig.merchantPrivacyPolicyURL = NSURL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full") as URL?
        payPalConfig.merchantUserAgreementURL = NSURL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")! as URL
        
        
        
        payPalConfig.languageOrLocale = NSLocale.preferredLanguages[0]
        
        // Setting the payPalShippingAddressOption property is optional.
        //
        // See PayPalConfiguration.h for details.
        
        payPalConfig.payPalShippingAddressOption = .payPal;
        
        print("PayPal iOS SDK Version: \(PayPalMobile.libraryVersion())")
        
    }
    
    func clickOnshareMethod(sender: UIBarButtonItem)
    {
         let filePathToUpload = URL(fileURLWithPath: Bundle.main.path(forResource: resourceName, ofType: "pdf")!)
        let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [filePathToUpload], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView=self.view
        present(activityViewController, animated: true, completion: nil)
        
        
        //self.uploadPdfonicloudDrive()
        
        
        
        
//        let actionSheetController: UIAlertController = UIAlertController(title: "Information", message: "This templet will be uploaded on your iCloud drive after purchasing, press OK to proceed.", preferredStyle: .alert)
//        let OkAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) { action -> Void in
//            //Just dismiss the action sheet
//        }
//        let cancelAction: UIAlertAction = UIAlertAction(title: "CANCEL", style: .cancel) { action -> Void in
//            //Just dismiss the action sheet
//        }
//         actionSheetController.addAction(OkAction)
//        actionSheetController.addAction(cancelAction)
//        self.present(actionSheetController, animated: true, completion: nil)
     //  self.purchaseWithPaypal()
        
    }
    
    
    func uploadPdfonicloudDrive()
    {
        //  Converted with Swiftify v1.0.6314 - https://objectivec2swift.com/
        //Create the file path of the document to upload
        let filePathToUpload = URL(fileURLWithPath: Bundle.main.path(forResource: "Core Value Statement", ofType: "pdf")!)
        
        //Create a object of document picker view and set the mode to Export
        let docPicker = UIDocumentPickerViewController(url: filePathToUpload, in: .exportToService)
        //Set the delegate
        docPicker.delegate = self as UIDocumentPickerDelegate
        //present the document picker
        present(docPicker, animated: true, completion: { _ in })

    }
   
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        if controller.documentPickerMode == .exportToService {
            // Called when user uploaded the file - Display success alert
            DispatchQueue.main.async(execute: {() -> Void in
                let alertMessage: String = "Successfully uploaded file \(url.lastPathComponent)"
                let alertController = UIAlertController(title: "UIDocumentView", message: alertMessage, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alertController, animated: true, completion: { _ in })
            })
        }
    }
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
    }
    
    
    
    var environment:String = PayPalEnvironmentNoNetwork {
    willSet(newEnvironment) {
    if (newEnvironment != environment) {
    PayPalMobile.preconnect(withEnvironment: newEnvironment)
    }
    }
    }
    

    #if HAS_CARDIO
    // You should use the PayPal-iOS-SDK+card-Sample-App target to enable this setting.
    // For your apps, you will need to link to the libCardIO and dependent libraries. Please read the README.md
    // for more details.
    var acceptCreditCards: Bool = true {
    didSet {
    payPalConfig.acceptCreditCards = acceptCreditCards
    }
    }
    #else
    var acceptCreditCards: Bool = false {
    didSet {
    payPalConfig.acceptCreditCards = acceptCreditCards
    }
    }
    #endif
    
    
    var resultText = "" // empty
    var payPalConfig = PayPalConfiguration() // default
    
    
  //  @IBAction func payByPaypal(sender: UIButton) {
    func purchaseWithPaypal(){
    print("integrate paypal here")
    resultText = ""
    
    // Note: For purposes of illustration, this example shows a payment that includes
    //       both payment details (subtotal, shipping, tax) and multiple items.
    //       You would only specify these if appropriate to your situation.
    //       Otherwise, you can leave payment.items and/or payment.paymentDetails nil,
    //       and simply set payment.amount to your total charge.
    
        
    // Optional: include multiple items
    let item1 = PayPalItem(name: "Old jeans with holes", withQuantity: 2, withPrice: NSDecimalNumber(string: "84.99"), withCurrency: "USD", withSku: "Hip-0037")
    let item2 = PayPalItem(name: "Free rainbow patch", withQuantity: 1, withPrice: NSDecimalNumber(string: "0.00"), withCurrency: "USD", withSku: "Hip-00066")
    let item3 = PayPalItem(name: "Long-sleeve plaid shirt (mustache not included)", withQuantity: 1, withPrice: NSDecimalNumber(string: "37.99"), withCurrency: "USD", withSku: "Hip-00291")
    
    let items = [item1, item2, item3]
    let subtotal = PayPalItem.totalPrice(forItems: items)
    
    // Optional: include payment details
    let shipping = NSDecimalNumber(string: "5.99")
    let tax = NSDecimalNumber(string: "2.50")
    let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
    
    let total = subtotal.adding(shipping).adding(tax)
 
   
    let payment = PayPalPayment(amount: total, currencyCode: "AUD", shortDescription: "My Shop", intent: .sale)
    
    payment.items = items
    payment.paymentDetails = paymentDetails
    
    if (payment.processable) {
    let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
    present(paymentViewController!, animated: true, completion: nil)
    }
    else {
    // This particular payment will always be processable. If, for
    // example, the amount was negative or the shortDescription was
    // empty, this payment wouldn't be processable, and you'd want
    // to handle that here.
    print("Payment not processalbe: \(payment)")
    }
    }
    
    
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
    print("PayPal Payment Cancelled")
    resultText = ""
    // successView.hidden = true
    paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
    print("PayPal Payment Success !")
      
        
    paymentViewController.dismiss(animated: true, completion: { () -> Void in
    // send completed confirmaion to your server
      print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
    
    self.resultText = completedPayment.description
    self.showSuccess(inputString: completedPayment.confirmation as NSObject)
    })
    }
    
    func showSuccess(inputString: NSObject) {
    
    print("this is done successfully :) ")
    print("send this to server \(inputString)")
    }
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
