//
//  QuestionnaireViewController.swift
//  Jolt
//
//  Created by ChawtechSolutions on 27/02/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit


class QuestionnaireViewController: UIViewController {
    
    var marrStudentData : NSMutableArray!
    @IBOutlet var lbl_firstQue: UILabel!
    var databasePath = String()
    @IBOutlet var lbl_secondQue: UILabel!
    
    @IBOutlet var radio1: UIButton!
    @IBOutlet var radio2: UIButton!
    @IBOutlet var radio3: UIButton!
    @IBOutlet var radio4: UIButton!
//    @IBOutlet var radio5: UIButton!
//    @IBOutlet var radio6: UIButton!
//    @IBOutlet var radio7: UIButton!
//    @IBOutlet var radio8: UIButton!
    @IBOutlet var btn_next: UIButton!
    @IBOutlet var btn_prev: UIButton!
    var activityIndicatorView: ActivityIndicatorView!
    var paging = Int()
    var paging_decisive = Int()
    var category = String()
    var firstAnswer = String()
    var secondAnswer = String()
    @IBOutlet var view_firstRadio: UIView!
   // @IBOutlet var view_secondRadio: UIView!
    var sendQuestionArr = NSMutableArray()
    var QuestionArr = NSMutableArray()
    var sendAnswerArr = NSMutableArray()
    var answerArr = NSMutableArray()
    var questionaireArr = NSMutableArray()
    var displaySelectedRadio : Bool = false
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
    }
    func createCustomRadioButtons()
    {
        self.radio1.isSelected = true
        self.radio1.tag = 0
        self.radio1.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        self.radio1.setBackgroundImage(UIImage(named: "checked.png"), for: .selected)
        self.radio1.addTarget(self, action: #selector(self.firstViewradiobuttonSelected), for: .touchUpInside)
        
        self.radio2.tag = 1
        self.radio2.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        self.radio2.setBackgroundImage(UIImage(named: "checked.png"), for: .selected)
        self.radio2.addTarget(self, action: #selector(self.firstViewradiobuttonSelected), for: .touchUpInside)
        
        self.radio3.tag = 2
        self.radio3.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        self.radio3.setBackgroundImage(UIImage(named: "checked.png"), for: .selected)
        self.radio3.addTarget(self, action: #selector(self.firstViewradiobuttonSelected), for: .touchUpInside)
        
        self.radio4.tag = 3
        self.radio4.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        self.radio4.setBackgroundImage(UIImage(named: "checked.png"), for: .selected)
        self.radio4.addTarget(self, action: #selector(self.firstViewradiobuttonSelected), for: .touchUpInside)
//        
//        self.radio5.isSelected = true
//        self.radio5.tag = 4
//        self.radio5.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
//        self.radio5.setBackgroundImage(UIImage(named: "checked.png"), for: .selected)
//        self.radio5.addTarget(self, action: #selector(self.secondViewradiobuttonSelected), for: .touchUpInside)
//        
//        self.radio6.tag = 5
//        self.radio6.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
//        self.radio6.setBackgroundImage(UIImage(named: "checked.png"), for: .selected)
//        self.radio6.addTarget(self, action: #selector(self.secondViewradiobuttonSelected), for: .touchUpInside)
//        
//        self.radio7.tag = 6
//        self.radio7.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
//        self.radio7.setBackgroundImage(UIImage(named: "checked.png"), for: .selected)
//        self.radio7.addTarget(self, action: #selector(self.secondViewradiobuttonSelected), for: .touchUpInside)
//        
//        
//        self.radio8.tag = 7
//        self.radio8.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
//        self.radio8.setBackgroundImage(UIImage(named: "checked.png"), for: .selected)
      //  self.radio8.addTarget(self, action: #selector(self.secondViewradiobuttonSelected), for: .touchUpInside)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.title = "DECISIVE"
        paging = 0
        firstAnswer = "1"
        secondAnswer = "1"
        btn_next.layer.cornerRadius = btn_next.frame.size.height/2
        btn_prev.alpha = 0.5
          btn_prev.isUserInteractionEnabled = false
        btn_prev.layer.cornerRadius = btn_next.frame.size.height/2
        self.createCustomRadioButtons()
        category = "DECISIVE"
        answerArr.removeAllObjects()
        QuestionArr.removeAllObjects()
        
        self.callGetquestioniereMethod(category:"DECISIVE")
        
        
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentsDirectory:AnyObject = paths[0] as AnyObject
        databasePath = documentsDirectory.appendingPathComponent(dataBaseName)
        _ =  self.deleteSqliteDatabaseRows()
    }
    
    
    //MARK:-  server call of questioniere method
    func callGetquestioniereMethod(category:String)
    {
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            
        }
        let apiCall = JsonApi()
        let parameters = [
            
            "user_id": UserDefaults.standard.object(forKey:"id")!,
            "category": category,
            "page": paging,
            
            ]
        print(parameters)
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalgetQuestionire, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            
            let success = result["success"] as! String
            
            if success == "true"
            {
                // set all key values in user defaults
                let data = result["data"] as? [[String: Any]]
                self.parseGetserviceProviderListData(dataArray: data!)
            }
        }
    }
    //  MARK:- parseGetserviceProviderListData
    func parseGetserviceProviderListData(dataArray:[[String: Any]])
    {
        
        DispatchQueue.main.async
            {
                self.activityIndicatorView.stopAnimating()
                
        }
        
        // let count =   deleteSqliteDatabaseRows()
        
        
        // quesDict.setValue(dataArray[i]["id"] as? String, forKey:String(i))
        //  QuestionArr.add(dataArray[i]["id"] as! String)
        
        
        
        if dataArray.count != 0
        {
            DispatchQueue.main.async{
              //  self.view_secondRadio.isHidden = false
            }
            
            self.saveSqliteDatabase(dataArray: dataArray)
            self.fetchquestionsandUpdateUi()
            
        }
        else
        {
            //            if category == "DECISIVE"
            //            {
            sendQuestionArr.removeAllObjects()
            sendAnswerArr.removeAllObjects()
            let createServerArr = fetchAllDatafromsqliteForSendingServer(category:self.category)
            
            var jsonObject :[String:String] = [:]
                var jsonObject1  :[String:String] = [:]
            for i in 0...createServerArr.count-1
            {
                 let assist:AssismentInfo = createServerArr.object(at:i) as! AssismentInfo
                let quesno = String(i)
                let que_id =  String(assist.questionid)
    
               
                let ansno = String(i)
                let ans_id = String(assist.answer)
               
                jsonObject[quesno] = que_id
                jsonObject1[ansno] = ans_id
            
            }
            print(jsonObject)
            print(jsonObject1)
       
            
            var dictPara :[String:AnyObject] = [:]
            dictPara["question_id"] = jsonObject as AnyObject?
            dictPara["rate"] = jsonObject1 as AnyObject?

            print(dictPara)
        self.callAssismentResultsMethod(dictPara: dictPara as NSDictionary)
            print(sendQuestionArr)
            print(sendAnswerArr)
        }
        
        print(QuestionArr)
        
        
    }
    func fetchquestionsandUpdateUi()
    {
        
        var assismentArr = NSMutableArray()
        assismentArr = self.fetchAllDatafromsqliteForDisplay()
        print(assismentArr)
//        if assismentArr.count == 2
//        {
            for i in 0...assismentArr.count-1
            {
                let assist:AssismentInfo = assismentArr.object(at:i) as! AssismentInfo
//                if lbl_firstQue.text == ""
//                {
                    DispatchQueue.main.sync{
                        self.lbl_firstQue.text =  "  " + assist.firstquestion
                         self.lbl_secondQue.text = "  " +  assist.secondquestion
                        // self.view_secondRadio.isHidden = true
//                    }
//                }
//                else
//                {
//                    DispatchQueue.main.sync{
//                        
//                        self.lbl_secondQue.text = "  " +  assist.secondquestion}
//                    // self.view_secondRadio.isHidden = false
               }
            }
            
           
//        }
//        else
//        {
//            let assist:AssismentInfo = assismentArr.object(at:0) as! AssismentInfo
//            if lbl_firstQue.text == ""
//            {
//                DispatchQueue.main.sync{
//                    lbl_firstQue.text =  "  " + assist.firstquestion
//                  //  self.view_secondRadio.isHidden = true
//                }
//            }
        
       // }
          }
    
    func checkdataAlreadyExist(questionid:String)-> Int
    {
        var count = Int()
        let contactDB = FMDatabase(path: databasePath as String)
        contactDB?.open()
        let countQuerySQL = "SELECT * FROM ASSISMENT_TABLE WHERE QUESTIONID = '\(questionid)' AND CATEGORY = '\(category)'"
        
        let result = contactDB?.executeQuery(countQuerySQL, withArgumentsIn:[])
        
        if result != nil
        {
            if (result?.next())!
            {
                count = Int((result?.int(forColumnIndex: 0))!)
            }
        }
        contactDB?.close()
        //        }
        //
        return count
        
    }
    func deleteSqliteDatabaseRows()-> Int32
    {
        var count = Int32()
        let contactDB = FMDatabase(path: databasePath as String)
        contactDB?.open()
        
        let querySQL = "DELETE FROM ASSISMENT_TABLE"
        
        contactDB?.executeUpdate(querySQL, withArgumentsIn:nil)
        
        
        let countQuerySQL = "SELECT * FROM ASSISMENT_TABLE"
        
        let result = contactDB?.executeQuery(countQuerySQL, withArgumentsIn:[])
        
        if result != nil
        {
            if (result?.next())!
            {
                count = (result?.int(forColumnIndex: 0))!
            }
        }
        contactDB?.close()
        //        }
        //
        return count
    }
    
    func updateAnswerinSqliteDatabase(questionId:String,answer:String)
    {
        
        let contactDB = FMDatabase(path: databasePath as String)
        contactDB?.open()
    
        
      //let updateSQL = "UPDATE ASSISMENT_TABLE SET ANSWER=? WHERE QUESTIONID=?", values: [answer,questionId]"
      // let updateSQL = "UPDATE ASSISMENT_TABLE SET ANSWER ='\(answer)' WHERE QUESTIONID = '\(questionId)')"
      //  let result = contactDB?.executeUpdate(updateSQL,withArgumentsIn: nil)
        // let results:FMResultSet? = contactDB?.executeQuery(updateSQL,withArgumentsIn: nil)
         let result = contactDB?.executeUpdate("UPDATE ASSISMENT_TABLE SET ANSWER=? WHERE QUESTIONID=? AND CATEGORY =?", withArgumentsIn: [answer, questionId,category])
        
        if !result! {
            print("Failed to update contact")
            
        } else {
            print("update contact")
            
        }
        
//        if results != nil {
//            
//            
//            while (results?.next())! {
//                
//               let answer = (results?.string(forColumn: "answer"))!
//                print(answer)
//            }
//        }
//       

        
        contactDB?.close()
    }
    
    func getQuestionIdFromSqliteDatabase(question:String) -> String
    {
        var questionid = String()
        let contactDB = FMDatabase(path: databasePath as String)
        contactDB?.open()
        
        
//        let getSQL = "SELECT * FROM ASSISMENT_TABLE WHERE QUESTION ='\(question)')"
//        let results:FMResultSet? = contactDB?.executeQuery(getSQL,
//                        withArgumentsIn: nil)
        
        let querySQL = "SELECT * FROM ASSISMENT_TABLE WHERE FIRSTQUESTION = '\(question)'AND CATEGORY = '\(category)'"
        let results:FMResultSet? = contactDB?.executeQuery(querySQL,withArgumentsIn: nil)
        if results != nil {
            
        
        while (results?.next())! {
            
            questionid = (results?.string(forColumn: "questionid"))!
                   }
        }
        contactDB?.close()
        return questionid
    }
    
    func saveSqliteDatabase(dataArray:[[String: Any]])
    {
        
        let contactDB = FMDatabase(path: databasePath as String)
        contactDB?.open()
        
        for i in 0...dataArray.count - 1   {
            
            let count =  self.checkdataAlreadyExist(questionid:dataArray[i]["id"] as! String)
            if count == 0
            {
                
                let insertSQL = "INSERT INTO ASSISMENT_TABLE (questionid, firstquestion,secondquestion,category,page,answer) VALUES ('\(dataArray[i]["id"]!)', '\(dataArray[i]["question1"]!)','\(dataArray[i]["question2"]!)','\(dataArray[i]["category"]!)','\(paging)','\("")')"
                let result = contactDB?.executeUpdate(insertSQL,withArgumentsIn: nil)
                
                if !result! {
                    print("Failed to add contact")
                    
                } else {
                    print("add contact")
                    
                }
                
            }
        }
        contactDB?.close()
        
    }
    func fetchAllDatafromsqliteForDisplay() -> NSMutableArray
    {
        let arr=NSMutableArray()
        arr.removeAllObjects()
        
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentsDirectory:AnyObject = paths[0] as AnyObject
        let  databasePath = documentsDirectory.appendingPathComponent(dataBaseName) as String
        
        let contactDB = FMDatabase(path: databasePath as String)
        
        if (contactDB?.open())! {
            // let querySQL = "SELECT * FROM ASSISMENT_TABLE"
            let querySQL = "SELECT * FROM ASSISMENT_TABLE WHERE PAGE = '\(paging)' AND CATEGORY = '\(category)'"
            let results:FMResultSet? = contactDB?.executeQuery(querySQL,
            withArgumentsIn: nil)
            while (results?.next())! {
                
                let assismentInfo : AssismentInfo = AssismentInfo()
                assismentInfo.answer = (results?.string(forColumn: "answer"))!
                assismentInfo.questionid = (results?.string(forColumn: "questionid"))!
                assismentInfo.firstquestion = (results?.string(forColumn: "firstquestion"))!
                assismentInfo.secondquestion = (results?.string(forColumn: "secondquestion"))!
                assismentInfo.category =  (results?.string(forColumn: "category"))!
                arr.add(assismentInfo)
                
                //               arr.add((results?.string(forColumn: "page"))!)
                //                arr.add((results?.string(forColumn: "questionid"))!)
                //                arr.add((results?.string(forColumn: "question"))!)
                //                arr.add((results?.string(forColumn: "category"))!)
                //                print("Record Found")
            }
            
            contactDB?.close()
        } else {
            print("Error: \(String(describing: contactDB?.lastErrorMessage()))")
        }
        
        return arr
    }
    func fetchAllDatafromsqliteForSendingServer(category:String) -> NSMutableArray
    {
        let arr=NSMutableArray()
       // arr.removeAllObjects()
        
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentsDirectory:AnyObject = paths[0] as AnyObject
        let  databasePath = documentsDirectory.appendingPathComponent(dataBaseName) as String
        
        let contactDB = FMDatabase(path: databasePath as String)
        
        if (contactDB?.open())! {
            // let querySQL = "SELECT * FROM ASSISMENT_TABLE"
            let querySQL = "SELECT * FROM ASSISMENT_TABLE WHERE CATEGORY = '\(category)'"
            let results:FMResultSet? = contactDB?.executeQuery(querySQL,
                                                               withArgumentsIn: nil)
            while (results?.next())! {
                
                let assismentInfo : AssismentInfo = AssismentInfo()
                assismentInfo.answer = (results?.string(forColumn: "answer"))!
                assismentInfo.questionid = (results?.string(forColumn: "questionid"))!
                assismentInfo.firstquestion = (results?.string(forColumn: "firstquestion"))!
                  assismentInfo.secondquestion = (results?.string(forColumn: "secondquestion"))!
                assismentInfo.category =  (results?.string(forColumn: "category"))!
                arr.add(assismentInfo)
                
                }
                       contactDB?.close()
        } else {
            print("Error: \(String(describing: contactDB?.lastErrorMessage()))")
        }
        
        return arr
    }

    
    //    func inserRecodInSqlite(category:String,id:String,question:String,page:String)
    //    {
    //        let assismentInfo: AssismentInfo = AssismentInfo()
    //        assismentInfo.category = category
    //        assismentInfo.id = id
    //        assismentInfo.question = question
    //        assismentInfo.page = page
    //
    //        let isInserted = ModelManager.getInstance().addStudentData(assismentInfo)
    //        if isInserted {
    //            Util.invokeAlertMethod("", strBody: "Record Inserted successfully.", delegate: nil)
    //        } else {
    //            Util.invokeAlertMethod("", strBody: "Error in inserting record.", delegate: nil)
    //        }
    //
    //
    //    }
    //
    //    func getStudentData()
    //    {
    //        marrStudentData = NSMutableArray()
    //        marrStudentData = ModelManager.getInstance().getAllStudentData()
    //        //tbStudentData.reloadData()
    //    }
    
    
    @IBAction func btn_prevAction(_ sender: Any)
    {
        lbl_firstQue.text = ""
        lbl_secondQue.text = ""
     //   self.view_secondRadio.isHidden = false
        
        var assismentArr = NSMutableArray()
//        assismentArr = self.fetchAllDatafromsqliteForDisplay()
//        print(assismentArr)

        
      //  if paging_decisive == 0 {
            paging = paging - 1
        assismentArr = self.fetchAllDatafromsqliteForDisplay()
        print(assismentArr)
        if assismentArr.count > 0 {
            
         

        for i in 0...assismentArr.count-1
        {
             displaySelectedRadio = true
            let assist:AssismentInfo = assismentArr.object(at:i) as! AssismentInfo
            //if lbl_firstQue.text == ""
            if (lbl_firstQue.text == "") && (lbl_secondQue.text == "")
            {
                lbl_firstQue.text =  "  " + assist.firstquestion
               // self.firstViewradiobuttonSelected(Int(assist.answer)! - 1)
                self.firstViewradiobuttonSelected(Int(assist.answer)! - 1 as Any)
                self.lbl_secondQue.text = "  " +  assist.secondquestion
               
            }
            else
            {
               // self.lbl_secondQue.text = "  " +  assist.secondquestion
            // self.secondViewradiobuttonSelected(Int(assist.answer)!  + 3 as Any)
            }
            }
            if paging == 0  && paging_decisive == 0
            {
                btn_prev.alpha = 0.5
                btn_prev.isUserInteractionEnabled = false
            }
  
        }
        else
        {
            self.title = "DECISIVE"
            self.category = "DECISIVE"
             paging = paging_decisive
            paging = paging - 1
            assismentArr = self.fetchAllDatafromsqliteForDisplay()
            print(assismentArr)
             paging_decisive = 0
            for i in 0...assismentArr.count-1
            {
            displaySelectedRadio = true
            let assist:AssismentInfo = assismentArr.object(at:i) as! AssismentInfo
            //if lbl_firstQue.text == ""
            if (lbl_firstQue.text == "") && (lbl_secondQue.text == "")
            {
                lbl_firstQue.text =  "  " + assist.firstquestion
                // self.firstViewradiobuttonSelected(Int(assist.answer)! - 1)
                self.firstViewradiobuttonSelected(Int(assist.answer)! - 1 as Any)
                self.lbl_secondQue.text = "  " +  assist.secondquestion
                
            }
            else
            {
                // self.lbl_secondQue.text = "  " +  assist.secondquestion
                // self.secondViewradiobuttonSelected(Int(assist.answer)!  + 3 as Any)
                } }
            
//            if paging == 0
//            {
//                btn_prev.alpha = 0.5
//                btn_prev.isUserInteractionEnabled = false
//            }
            
        }

        
        
         displaySelectedRadio = false
    }
    
    @IBAction func btn_nextAction(_ sender: Any)
    {
        //    let ansDict = NSMutableDictionary()
        //    ansDict.setValue(firstAnswer, forKey:"0")
        //    answerArr.add(ansDict.mutableCopy())
        btn_prev.alpha = 1
        btn_prev.isUserInteractionEnabled = true
        
        if (lbl_firstQue.text != "") && (lbl_secondQue.text != "")
        {
          let  firstLabelText = lbl_firstQue.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
          let   secondLabelText = lbl_secondQue.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            
          var questionid =  self.getQuestionIdFromSqliteDatabase(question:firstLabelText)
          self.updateAnswerinSqliteDatabase(questionId: questionid, answer: firstAnswer)
             questionid =  self.getQuestionIdFromSqliteDatabase(question:secondLabelText)
         self.updateAnswerinSqliteDatabase(questionId: questionid, answer: secondAnswer)
        }
        else
        {
            let  firstLabelText = lbl_firstQue.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            let questionid = self.getQuestionIdFromSqliteDatabase(question:firstLabelText)
             self.updateAnswerinSqliteDatabase(questionId: questionid, answer: firstAnswer)
        }
        
        
        paging = paging + 1
        
        if category == "REACTIVE"
        {
            answerArr.add(firstAnswer)
//            if view_secondRadio.isHidden == true
//            {
//                
//            }
//            else
//            {
//                answerArr.add(secondAnswer)
//            }
            
            self.callGetquestioniereMethod(category:"REACTIVE")
        }
        else
        {
            answerArr.add(firstAnswer)
//            if view_secondRadio.isHidden == true
//            {
//                
//            }
//            else
//            {
//                
//                answerArr.add(secondAnswer)
//            }
            
            
            self.callGetquestioniereMethod(category: "DECISIVE")
        }
       // lbl_firstQue.text = ""
       // lbl_secondQue.text = ""
        
         displaySelectedRadio = false
        print(answerArr)
    }
    
    
    //MARK:-  server call of questioniere method
    func callAssismentResultsMethod(dictPara: NSDictionary)
    {
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            
        }
        
      
        
        var jsonString = String()
//        let jsonObject: NSMutableDictionary = NSMutableDictionary()
//        
//        jsonObject.setValue(UserDefaults.standard.object(forKey:"id"), forKey:"user_id")
//        jsonObject.setValue(txtf_name.text, forKey: "name")
//        jsonObject.setValue(txtf_email.text, forKey: "email")
//        jsonObject.setValue(txtf_mobile.text, forKey: "mobile")
//        jsonObject.setValue(txtf_degree.text, forKey: "graduation")
//        jsonObject.setValue(txtf_age.text!, forKey: "age_in_year")
//        jsonObject.setValue(txtf_city.text!, forKey: "city")
//        jsonObject.setValue(txtf_password.text!, forKey: "password")
//        
//
        let jsonData: NSData
        
        do {
            jsonData = try JSONSerialization.data(withJSONObject: dictPara, options: JSONSerialization.WritingOptions()) as NSData
            jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String
            print("json string = \(jsonString)")
            
            
        } catch _ {
            print ("JSON Failure")
        }
        

        let apiCall = JsonApi()
        let parameters = [
            
            "user_id": UserDefaults.standard.object(forKey:"id")!,
            "key_answer": dictPara,
            
            
            ]
        print(parameters)
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalAssismentResultUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            
            let success = result["success"] as! String
            
            if success == "true"
            {
                
                if self.category == "DECISIVE"
                {
                    DispatchQueue.main.async
                        {
                            self.activityIndicatorView.stopAnimating()
                            self.title = "REACTIVE"
                    }
                    self.paging_decisive = self.paging
                    self.paging = 0
                    self.category = "REACTIVE"
                    self.answerArr.removeAllObjects()
                    self.QuestionArr.removeAllObjects()
                    self.callGetquestioniereMethod(category:"REACTIVE")
                }
                else
                {
                    DispatchQueue.main.async
                        {
                            self.activityIndicatorView.stopAnimating()
                            
                            
                            let storyboard = UIStoryboard(name:"Main", bundle: nil)
                            let vc  = storyboard.instantiateViewController(withIdentifier:"GraphViewController") as! GraphViewController
                            self.navigationController?.pushViewController(vc, animated: true)
                    }}
                
            }
        }
    }
    
    func firstViewradiobuttonSelected(_ sender: Any) {
       // switch (sender as AnyObject).tag {
        var tagValue = Int()
        if displaySelectedRadio == true {
            tagValue = sender as! Int
        }
        else
        {
         tagValue = (sender as AnyObject).tag!
        }
        
        
         switch (tagValue) {
        case 0:
            firstAnswer = "1"
            self.radio1.isSelected = true
            self.radio2.isSelected = false
            self.radio3.isSelected = false
            self.radio4.isSelected = false
            
            break
        case 1:
            firstAnswer = "2"
            self.radio2.isSelected = true
            self.radio1.isSelected = false
            self.radio3.isSelected = false
            self.radio4.isSelected = false
            
            break
        case 2:
            firstAnswer = "3"
            self.radio3.isSelected = true
            self.radio1.isSelected = false
            self.radio2.isSelected = false
            self.radio4.isSelected = false
            
            break
        case 3:
            firstAnswer = "4"
            self.radio4.isSelected = true
            self.radio1.isSelected = false
            self.radio2.isSelected = false
            self.radio3.isSelected = false
            
            break
        default:
            break
        }
        
    }
//    func secondViewradiobuttonSelected(_ sender: Any) {
//        //switch (sender as AnyObject).tag {
//        var tagValue = Int()
//          if displaySelectedRadio == true {
//            tagValue = sender as! Int
//        }
//        else
//        {
//            tagValue = (sender as AnyObject).tag!
//        }
//        switch (tagValue) {
//        case 4:
//            secondAnswer = "1"
//            self.radio5.isSelected = true
//            self.radio6.isSelected = false
//            self.radio7.isSelected = false
//            self.radio8.isSelected = false
//            break
//        case 5:
//            secondAnswer = "2"
//            self.radio6.isSelected = true
//            self.radio5.isSelected = false
//            self.radio7.isSelected = false
//            self.radio8.isSelected = false
//            break
//        case 6:
//            secondAnswer = "3"
//            self.radio7.isSelected = true
//            self.radio5.isSelected = false
//            self.radio6.isSelected = false
//            self.radio8.isSelected = false
//            
//            break
//        case 7:
//            secondAnswer = "4"
//            self.radio8.isSelected = true
//            self.radio5.isSelected = false
//            self.radio6.isSelected = false
//            self.radio7.isSelected = false
//            
//            break
//            
//        default:
//            break
//        }
//    }
//    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
