//
//  JobBoardTVCell.swift
//  Jolt
//
//  Created by chawtech solutions on 4/7/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class JobBoardTVCell: UITableViewCell {
// @IBOutlet weak var image_view: UIImageView!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_subtitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
