//
//  ProfileViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 3/16/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

     @IBOutlet weak var imgv_profile: UIImageView!
     @IBOutlet weak var tbl_profile: UITableView!
   // var profileArr = ["COACH","JOB BOARD","ASSESSMENT TEST", "PLANNING TOOLS","rg"]
    var profileImages_array = ["phone", "email", "city", "degree","age"]
    var profileSubTitle_array = ["Mobile","Email","City", "Degree Level","Age"]
    var activityIndicatorView: ActivityIndicatorView!
    var  profileArr = NSMutableArray()
    @IBOutlet weak var lbl_username: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "PROFILE"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.callgetGetProfileMethod()
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        imgv_profile.layer.cornerRadius = imgv_profile.frame.size.height/2
        imgv_profile.clipsToBounds = true
        
        
    }

    
    @IBAction func btn_editProfileAction(_ sender: Any)
    {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"EditProfileViewController") as! EditProfileViewController
        self.navigationItem.title = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_LogoutAction(_ sender: Any)
    {
        DispatchQueue.main.async
            {

        let actionSheetController: UIAlertController = UIAlertController(title: "Are You Sure!", message: "Do You Want To Logout?", preferredStyle: .alert)
        let yesAction: UIAlertAction = UIAlertAction(title: "YES", style: .default) { action -> Void in
            //Just dismiss the action sheet
            self.callogoutMethod()
        }
        let noAction: UIAlertAction = UIAlertAction(title: "NO", style: .default) { action -> Void in
            //Just dismiss the action sheet
        }
        actionSheetController.addAction(yesAction)
        actionSheetController.addAction(noAction)
        self.present(actionSheetController, animated: true, completion: nil)
        }
        
//         var viewControllerAlreadyPushed: Bool = false
//        for controller: UIViewController in (self.navigationController?.viewControllers)! {
//            if (controller is LoginViewController) {
//                viewControllerAlreadyPushed = true
//                _ = self.navigationController?.popViewController(animated: false)
//            }
//        }
//        if !viewControllerAlreadyPushed {
//            let storyboard = UIStoryboard(name:"Main", bundle: nil)
//            let vc  = storyboard.instantiateViewController(withIdentifier:"LoginViewController") as! LoginViewController
//            self.navigationController?.pushViewController(vc, animated: false)
//        }


    }
    // MARK:- Server call of getService Provider Method
    func callogoutMethod()
    {
        
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            // UIApplication.shared.beginIgnoringInteractionEvents()
        }
        
        let apiCall = JsonApi()
        let parameters = [
            
            "user_id": UserDefaults.standard.object(forKey: "id"),
            
            
            
            ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalLogoutUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            
            let success = result["success"] as! String
            let message = result["message"] as! String
            
            
            if success == "true"
            {
                DispatchQueue.main.async
                    {

                 self.activityIndicatorView.stopAnimating()
                UserDefaults.standard.removeObject(forKey:"email")
                UserDefaults.standard.removeObject(forKey:"name")
                UserDefaults.standard.removeObject(forKey:"registerDate")
                UserDefaults.standard.removeObject(forKey:"id")
                UserDefaults.standard.removeObject(forKey:"password")
                
                let appDelegate: AppDelegate? = UIApplication.shared.delegate as! AppDelegate?
                appDelegate?.callLoginControllerAfterLogout()
                }
            }
            else
            {
                DispatchQueue.main.async
                {
                        
                    self.activityIndicatorView.stopAnimating()
                    self.displayAlertWithMessage(viewController:self,title:"Alert!" ,message:message)
                        
                }
                
            }
        }
    }

    // MARK:- Server call of getService Provider Method
    func callgetGetProfileMethod()
    {
        
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            // UIApplication.shared.beginIgnoringInteractionEvents()
        }
        
        let apiCall = JsonApi()
        let parameters = [
            
            "user_id": UserDefaults.standard.object(forKey: "id"),
           
            
            
            ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalgetProfileUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            
            let success = result["success"] as! String
            let message = result["message"] as! String
            
            
            if success == "true"
            {
                // parse data from server
                let Arr = result["data"] as! [String: Any]
                self.parseGetprofileData(dataArray: [Arr])
            }
            else
            {
                DispatchQueue.main.async
                    {
                       
                        self.displayAlertWithMessage(viewController:self,title:"Alert!" ,message:message)
                        
                }
                
            }
        }
    }
    //  MARK:- parseGetserviceProviderListData
    func parseGetprofileData(dataArray:[[String: Any]])
    {
     
        profileArr.removeAllObjects()
        
        
        if let mobile = dataArray[0]["mobile"] as? String
        {
             UserDefaults.standard.setValue(mobile, forKey: "mobile")
            profileArr.add(mobile)
        }
        
        if let email = dataArray[0]["email"] as? String
        {
            profileArr.add(email)
             UserDefaults.standard.setValue(email, forKey: "email")
        }
        
        if let city = dataArray[0]["city"] as? String
        {
             UserDefaults.standard.setValue(city, forKey: "city")
            profileArr.add(city)
        }

    
        if let graduation = dataArray[0]["graduation"] as? String
        {
             UserDefaults.standard.setValue(graduation, forKey: "graduation")
            profileArr.add(graduation)
        }

        if let age = dataArray[0]["age_in_year"] as? String
        {
            UserDefaults.standard.setValue(age, forKey: "age_in_year")
            profileArr.add(age)
        }

        if let name = dataArray[0]["name"] as? String
        {
            UserDefaults.standard.setValue(name, forKey: "name")
            DispatchQueue.main.async {
                  self.lbl_username.text = name
            }
          
        }
        if let photo = dataArray[0]["photo"] as? String
        {
            UserDefaults.standard.setValue(photo, forKey: "profile-pic")
            
            DispatchQueue.main.async {
                if photo != ""
                {
                    let imgURL = URL(string:photo)
                    self.imgv_profile.kf.setImage(with: imgURL)
                }
                    
                    
                else
                {
                    self.imgv_profile.image = UIImage.init(named:"profile_placholder")
                }

            }
            
        }
       
        DispatchQueue.main.async
            {
                self.activityIndicatorView.stopAnimating()
                self.tbl_profile.reloadData()
                
        }
    }

    
    //MARK:- tableview delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profileArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(
            withIdentifier: "cell",
            for: indexPath)as! ProfileTVCell
        
        
      // let dict = profileArr[indexPath.row] as? Dictionary<String,String>
      //  cell.backgroundColor = UIColor.white
        
        cell.lbl_title.text = profileArr[indexPath.row] as? String
        cell.lbl_subtitle.text = profileSubTitle_array[indexPath.row]
        cell.image_view.image = UIImage.init(named: profileImages_array[indexPath.row])
        
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 70
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
