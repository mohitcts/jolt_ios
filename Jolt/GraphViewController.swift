//
//  GraphViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 3/20/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class GraphViewController: UIViewController,RPRadarChartDelegate,RPRadarChartDataSource {
var colors = [Any]()
    var titles = [Any]()
    @IBOutlet var radio1: UIButton!
    @IBOutlet var radio2: UIButton!
    @IBOutlet var btn_next: UIButton!
    var lowDecisive =  Float()
    var highDecisive =  Float()
    var lowReactive =  Float()
    var highReactive =  Float()
    var perdonalityInfo = [String:AnyObject]()
    var perdonalityStyle =  String()
    
    @IBOutlet weak var lbl_personalityStyle: UILabel!
    
     var activityIndicatorView: ActivityIndicatorView!
    func createCustomRadioButtons()
    {
       // self.radio1.isSelected = true
        self.radio1.tag = 0
        self.radio1.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        self.radio1.setBackgroundImage(UIImage(named: "checked.png"), for: .selected)
        self.radio1.addTarget(self, action: #selector(self.radiobuttonSelected), for: .touchUpInside)
        
        self.radio2.tag = 1
        self.radio2.setBackgroundImage(UIImage(named: "unchecked.png"), for: .normal)
        self.radio2.setBackgroundImage(UIImage(named: "checked.png"), for: .selected)
        self.radio2.addTarget(self, action: #selector(self.radiobuttonSelected), for: .touchUpInside)
    }
    
    
    
    func radiobuttonSelected(_ sender: Any) {
        switch (sender as AnyObject).tag {
        case 0:
           
            self.radio1.isSelected = true
            self.radio2.isSelected = false
          // self.tabBarController!.selectedIndex = 0

           UIApplication.shared.open(NSURL(string:"http://35.167.21.236/transcendproserv/product-category/disc-assessment/")! as URL, options: [:], completionHandler: nil)
            
            break
        case 1:
            
            self.radio2.isSelected = true
            self.radio1.isSelected = false
            self.tabBarController!.selectedIndex = 1
            for controller: UIViewController in (self.navigationController?.viewControllers)! {
                if (controller is DashboardViewController) {
                  _ = self.navigationController?.popToViewController(controller, animated: true)
                }
            }
          
            
            break
        default:
            break
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "INFO GRAPH"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.createCustomRadioButtons()
        
        self.callgetPersonalityMethod()
        
        btn_next.layer.cornerRadius = btn_next.frame.size.height/2
    }
    @IBAction func btn_nextAction(_ sender: Any) {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"PersonalityViewController") as! PersonalityViewController
        self.title = ""
        vc.peronalityInfo = self.perdonalityInfo as NSDictionary
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func createRadarGraph()
    {
        let  rc : RPRadarChart!
        if self.view.frame.size.height > 570
        {
             rc = RPRadarChart(frame: CGRect(x: CGFloat(10), y: CGFloat(10), width: CGFloat(self.view.frame.size.width-20), height: CGFloat(self.view.frame.size.height/2+60)))
        }
        else
        {
               rc = RPRadarChart(frame: CGRect(x: CGFloat(10), y: CGFloat(0), width: CGFloat(self.view.frame.size.width-20), height: CGFloat(self.view.frame.size.height/2+40)))
        }
       
        rc.backgroundColor = UIColor.white
        colors = [UIColor.red, UIColor.blue, UIColor.orange, UIColor.purple, UIColor.green]
        titles = ["Low Reactive","Leader","High Decisive","Visionary","High Reactive","Stable","Low Decisive","Diligent"]
        rc.dataSource = self
        rc.delegate = self
        rc.backLineWidth = 0.5
        //  rc.showGuideNumbers != rc.showGuideNumbers
        self.view.addSubview(rc)
        //        rc.showGuideNumbers
        //        rc.drawGuideLines
        //         rc.fillArea
//        BOOL drawGuideLines;
//        BOOL showGuideNumbers;
//        BOOL showValues;
//        BOOL hasMultiDataSet;
       // rc.showValues = false

    }
    
    func numberOfSopkes(in chart: RPRadarChart) -> Int {
        return 8
    }
    // get number of datas
    
    func numberOfDatas(in chart: RPRadarChart) -> Int {
        return 1
    }
    // get max value for this radar chart
    
    func maximumValue(in chart: RPRadarChart) -> Float {
        return 8
    }
    // get title for each spoke
    
    func radarChart(_ chart: RPRadarChart, titleForSpoke atIndex: Int) -> String {
       // return "Spoke\(atIndex)"
        return titles[atIndex] as! String
    }
    
    func radarChart(_ chart: RPRadarChart, valueForData dataIndex: Int, forSpoke spokeIndex: Int) -> Float {
        var value = Float()
        value = 7
        if  perdonalityStyle != "" {
            value = 7
        }
        var data1: [Float] = [5, 5, 5, value, 5,5,5,5]
       // var data2: [Float] = [1, 2, 3, 4, 5,1,3,4]
        switch dataIndex {
        case 0:
            return data1[spokeIndex]
//        case 1:
//            return data2[spokeIndex]
        default:
            break
        }
       
        return 0
    }
    // get color legend for a specefic data
    
    func radarChart(_ chart: RPRadarChart, colorForData atIndex: Int) -> UIColor {
        return colors[atIndex] as! UIColor
    }
    // MARK: -- delegate for chart
    
    func radarChart(_ chart: RPRadarChart, lineTouchedForData dataIndex: Int, atPosition point: CGPoint) {
        print("Line \(dataIndex) touched at (\(point.x),\(point.y))")
    }
    
    
    //MARK:-  server call of login method
    func callgetPersonalityMethod()
    {
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            
        }
        let apiCall = JsonApi()
        let parameters = [
            
            "user_id": UserDefaults.standard.object(forKey: "id")!
        ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalgetPersonalityUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            let status = result["status"] as! String
            if status != ""
            {
                let success = result["success"] as! String
               
                
                print(status)
                if success == "true"
                {
                    DispatchQueue.main.sync {
                        self.activityIndicatorView.stopAnimating()
                         self.createRadarGraph()
                    
                   
                    // set all key values in user defaults
                    let data = result["data"] as (AnyObject)
                    if data.count != 0
                    {
                    let info = data.object(at:0) as (AnyObject)
                    self.perdonalityStyle = (info["personality_style"] as? String)!
                    self.lbl_personalityStyle.text = "Your Persaonality Style is " + self.perdonalityStyle
                    self.perdonalityInfo = (info) as! [String : AnyObject]
                    }
                }
                }
                else
                {
                    // call alert controller in main queue
                    DispatchQueue.main.async {
                        self.activityIndicatorView.stopAnimating()
                        self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:"")
                    }
                }
                
            }
            else
            {
                
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
