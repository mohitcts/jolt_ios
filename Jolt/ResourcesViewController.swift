
//
//  ResourcesViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 4/13/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit



class ResourcesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,PayPalPaymentDelegate

{
    

  var activityIndicatorView: ActivityIndicatorView!
    var selectedProductIndex: Int!
    
    var transactionInProgress = false
    @IBOutlet weak var tbl_resourcesList: UITableView!
    var productIDs: Array<String?> = []
    var plan_name  = String()
    var resource_title  = String()
  
    
    let resourceslistArr = NSMutableArray()// ["Detailed Business Plan","Core Value Statements","Invester's Pitch"]
    let resourcesAmountlistArr = ["$5.00","$5.00","$5.00"]
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = "RESOURCES"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func addFooter() {
        let v = UIView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(view.frame.size.width), height: CGFloat(0)))
        v.backgroundColor = UIColor.clear
        tbl_resourcesList.tableFooterView = v
    }
    

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //MARK: paypal Intergation inislization
        self.paypalIntegration()
        self.addFooter()
        self.callgetPaymentStatusMethod()
    }
    
    func paypalIntegration()
    {
        
        // Do any additional setup after loading the view.
        title = "PayPal SDK Demo"
        //successView.hidden = true
        
        // Set up payPalConfig
        print("this is credit card status \(acceptCreditCards)")
        payPalConfig.acceptCreditCards = true;
        payPalConfig.merchantName = "Awesome Shirts, Inc."
        payPalConfig.merchantPrivacyPolicyURL = NSURL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full") as URL?
        payPalConfig.merchantUserAgreementURL = NSURL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")! as URL
        
        
        
        payPalConfig.languageOrLocale = NSLocale.preferredLanguages[0]
        
        // Setting the payPalShippingAddressOption property is optional.
        //
        // See PayPalConfiguration.h for details.
        
        payPalConfig.payPalShippingAddressOption = .payPal;
        
        print("PayPal iOS SDK Version: \(PayPalMobile.libraryVersion())")
        
    }

    var environment:String = PayPalEnvironmentNoNetwork {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    
    
    #if HAS_CARDIO
    // You should use the PayPal-iOS-SDK+card-Sample-App target to enable this setting.
    // For your apps, you will need to link to the libCardIO and dependent libraries. Please read the README.md
    // for more details.
    var acceptCreditCards: Bool = true {
    didSet {
    payPalConfig.acceptCreditCards = acceptCreditCards
    }
    }
    #else
    var acceptCreditCards: Bool = false {
        didSet {
            payPalConfig.acceptCreditCards = acceptCreditCards
        }
    }
    #endif
    
    
    var resultText = "" // empty
    var payPalConfig = PayPalConfiguration() // default
    
    
    //  @IBAction func payByPaypal(sender: UIButton) {
    func purchaseWithPaypal(){
        print("integrate paypal here")
        resultText = ""
        
        
        let payPalConfig = PayPalConfiguration()
        
        payPalConfig.merchantName = "MacCrafters Software"
        // Note: For purposes of illustration, this example shows a payment that includes
        //       both payment details (subtotal, shipping, tax) and multiple items.
        //       You would only specify these if appropriate to your situation.
        //       Otherwise, you can leave payment.items and/or payment.paymentDetails nil,
        //       and simply set payment.amount to your total charge.
        
        // Optional: include multiple items
        let item1 = PayPalItem(name: "Business Plan", withQuantity: 1, withPrice: NSDecimalNumber(string: "5.00"), withCurrency: "USD", withSku: "Purchase")
      //  let item2 = PayPalItem(name: "Free rainbow patch", withQuantity: 1, withPrice: NSDecimalNumber(string: "0.00"), withCurrency: "USD", withSku: "Hip-00066")
      //  let item3 = PayPalItem(name: "Long-sleeve plaid shirt (mustache not included)", withQuantity: 1, withPrice: NSDecimalNumber(string: "37.99"), withCurrency: "USD", withSku: "Hip-00291")
        
        let items = [item1]
        let subtotal = PayPalItem.totalPrice(forItems: items)
        
        // Optional: include payment details
        let shipping = NSDecimalNumber(string: "0.00")
        let tax = NSDecimalNumber(string: "0.00")
        let paymentDetails = PayPalPaymentDetails(subtotal: subtotal, withShipping: shipping, withTax: tax)
        
        let total = subtotal.adding(shipping).adding(tax)
        
        let payment = PayPalPayment(amount: total, currencyCode: "AUD", shortDescription: "Jolt Career/Business", intent: .sale)
        
        payment.items = items
        payment.paymentDetails = paymentDetails
        
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            present(paymentViewController!, animated: true, completion: nil)
        }
        else {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
            print("Payment not processalbe: \(payment)")
        }
    }
    
    
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        resultText = ""
        // successView.hidden = true
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        
        
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            // send completed confirmaion to your server
            print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            
            self.resultText = completedPayment.description
            self.showSuccess(inputString: completedPayment.confirmation as NSDictionary)
        })
    }
    
    func showSuccess(inputString: NSDictionary) {
        
        print("this is done successfully :) ")
        print("send this to server \(inputString)")
        let response = inputString["response"]
        self.callupdatePaymentDetailMethod(inputString: response as! NSDictionary)
    }
    
    //MARK:-  server call of login method
    func callupdatePaymentDetailMethod(inputString:NSDictionary)
    {
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            
        }
        let apiCall = JsonApi()
        let parameters = [
            
            "plan_name": plan_name,
            "transaction_id": inputString["id"],
            "amount": "5",
            "payment_date": inputString["create_time"],
            "payment_status": inputString["state"],
            "user_id": UserDefaults.standard.object(forKey: "id")!
        ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalupdatePaymentDetailUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            let status = result["status"] as! String
            if status != ""
            {
                let success = result["success"] as! String
                let message = result["message"] as! String
                
                print(status)
                if success == "true"
                {
                    // set all key values in user defaults
                   
                                        // call next view controller in main queue
                    DispatchQueue.main.async {
                        self.activityIndicatorView.stopAnimating()
                                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                                    let vc  = storyboard.instantiateViewController(withIdentifier:"ResourceDetailViewController") as! ResourceDetailViewController
                                    self.navigationItem.title = ""
                                    vc.resourceName = self.plan_name
                                    vc.resourceTitle = self.resource_title
                                    self.navigationController?.pushViewController(vc, animated: true)

                    }
                }
                else
                {
                    // call alert controller in main queue
                    DispatchQueue.main.async {
                        self.activityIndicatorView.stopAnimating()
                        self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:message)
                    }
                }
                
            }
            else
            {
                DispatchQueue.main.async {
                    self.activityIndicatorView.stopAnimating()
                    self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:"Server Error! Please try again")
                }
                
            }
        }
    }
    //MARK:-  server call of login method
    func callgetPaymentStatusMethod()
    {
        
        var jsonObject :[String:String] = [:]
        
        jsonObject["0"] = "Detailed Business Plan"
        jsonObject["1"] = "Core Value Statement"
        jsonObject["2"] = "Investors Pitch"
        print(jsonObject)
        
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            
        }
        let apiCall = JsonApi()
        let parameters = [
            
            
            "user_id": UserDefaults.standard.object(forKey: "id")!,
            "plan_names":jsonObject
        ]
        print(parameters)
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalgetPaymentstatusUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            let status = result["status"] as! String
            if status != ""
            {
                let success = result["success"] as! String
                
                
                print(status)
                if success == "true"
                {
                    // set all key values in user defaults
                    let dataArr = result["data"] as! NSArray
                    self.parseGetstatusData(dataArray: dataArr)
                    
                }
                else
                {
                    // call alert controller in main queue
                    DispatchQueue.main.async {
                        self.activityIndicatorView.stopAnimating()
                        
                    }
                    
                    
                }
                
            }
            else
            {
                DispatchQueue.main.async {
                    self.activityIndicatorView.stopAnimating()
                    self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:"Server Error! Please try again")
                }
                
            }
        }
    }
    
    //  MARK:- parseGetserviceProviderListData
    func parseGetstatusData(dataArray:NSArray)
    {
        resourceslistArr.removeAllObjects()
        let dict = NSMutableDictionary()
        let arrJson = dataArray as! Array<Dictionary<String,Any>>
        for i in 0...dataArray.count-1
        {
            
            let Object = arrJson[i] as [String : AnyObject]
//            let payment_status = Object["payment_status"]as! String
//            let plan_name = Object["plan_name"]as! String
            
                    dict.setValue((Object["user_id"]), forKey: "user_id")
                    
                    
                    dict.setValue((Object["payment_status"] ), forKey: "payment_status")
                    dict.setValue((Object["plan_name"] ), forKey: "plan_name")
                    resourceslistArr.add(dict .mutableCopy())
                    
        
        }
        
        DispatchQueue.main.async
            {
                self.activityIndicatorView.stopAnimating()
                self.tbl_resourcesList.reloadData()
        }
        
        
    }
    

    //MARK:- tableview delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resourceslistArr.count
       ///return productsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(
            withIdentifier: "cell",
            for: indexPath)as! ResourcesTVCell
        
        
     let Object = resourceslistArr[indexPath.row] as! [String : AnyObject]
        
        cell.lbl_resourcename.text = Object["plan_name"] as? String
        
       let payment_status = Object["payment_status"] as? String
        if payment_status == "1"
        {
             cell.lbl_resourceamount.isHidden = true
        }
        else
        {
           
            cell.lbl_resourceamount.isHidden = false
            cell.lbl_resourceamount.text = resourcesAmountlistArr[indexPath.row]
        }
       
         return cell
        

    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
       let Cell = tableView.cellForRow(at: indexPath) as! ResourcesTVCell
        if indexPath.row == 0
        {
            plan_name = "Detailed Business Plan"
            resource_title = "DETAILED BUSINESS PLAN"
            if  Cell.lbl_resourceamount.isHidden == true
            {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier:"ResourceDetailViewController") as! ResourceDetailViewController
                self.navigationItem.title = ""
                vc.resourceName = self.plan_name
                vc.resourceTitle = self.resource_title
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else
            {
                 self.purchaseWithPaypal()
            }
        }
        if indexPath.row == 1
        {
             plan_name = "Core Value Statement"
             resource_title = "CORE VALUE STATEMENTS"
            if  Cell.lbl_resourceamount.isHidden == true
            {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier:"ResourceDetailViewController") as! ResourceDetailViewController
                self.navigationItem.title = ""
                vc.resourceName = self.plan_name
                vc.resourceTitle = self.resource_title
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else
            {
                self.purchaseWithPaypal()
            }

        }

        if indexPath.row == 2
        {
               plan_name = "Investors Pitch"
             resource_title = "INVESTERS PITCH"
            if  Cell.lbl_resourceamount.isHidden == true
            {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier:"ResourceDetailViewController") as! ResourceDetailViewController
                self.navigationItem.title = ""
                vc.resourceName = self.plan_name
                vc.resourceTitle = self.resource_title
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else
            {
                self.purchaseWithPaypal()
            }

        }

       
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
