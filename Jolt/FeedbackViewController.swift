//
//  FeedbackViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 5/19/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class FeedbackViewController: UIViewController,UITextFieldDelegate,UITextViewDelegate {
 @IBOutlet weak var txtf_username: UITextField!
     @IBOutlet weak var txtf_email: UITextField!
     @IBOutlet weak var txtf_comment: UITextView!
     @IBOutlet weak var btn_submit: UIButton!
     var activityIndicatorView: ActivityIndicatorView!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "WRITE US"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // TODO:- set textfield placeholder
        txtf_email.attributedPlaceholder = NSAttributedString(string:"Email", attributes: [NSForegroundColorAttributeName: UIColor.init(red: 121/255.0, green: 132/255.0, blue: 140/255.0, alpha: 1)])
        
        txtf_username.attributedPlaceholder = NSAttributedString(string:"User Name", attributes: [NSForegroundColorAttributeName: UIColor.init(red: 121/255.0, green: 132/255.0, blue: 140/255.0, alpha: 1)])
        
        txtf_comment.layer.borderColor = UIColor.lightGray.cgColor
        txtf_comment.layer.borderWidth = 2
        btn_submit.layer.cornerRadius = btn_submit.frame.size.height/2
        self.txtf_email.bs_setupErrorMessageView(withMessage: "Please Enter Email")
        self.txtf_username.bs_setupErrorMessageView(withMessage: "Please Enter User Name")
        txtf_email.delegate = self
        txtf_username.delegate = self
        txtf_comment.delegate = self

    }
    func textViewDidBeginEditing(_ textView: UITextView)
    {
        if txtf_comment.text == "Comment..." {
            txtf_comment.text = nil
            txtf_comment.textColor = UIColor.black
        }
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if txtf_comment.text == "" {
            txtf_comment.text = "Comment..."
            txtf_comment.textColor = UIColor.gray
        }
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.bs_hideError()
    }
    
    @IBAction func btn_submitAction(_ sender: Any) {
        
        if ((txtf_username.text?.characters.count)! > 0)
        {
            if ((txtf_email.text?.characters.count)! > 0)
            {
                if isValidEmail(testStr: txtf_email.text!)  == true
                {
                    if ((txtf_comment.text?.characters.count)! > 0)
                    {
                         self.callcontactusMethod()
                    }
                    else
                    {
                        self.displayAlertWithMessage(viewController:self, title: "Alert!", message: "Please write a comments")
                        
                    }
                }
                else
                {
                    self.txtf_email.bs_setupErrorMessageView(withMessage: "Please Enter Valid  Email")
                    self.txtf_email.bs_showError()
                    
                }

                
            }
            else
            {
                
                self.txtf_email.bs_showError()
                
            }
        }
        else
        {
            self.txtf_username.bs_showError()
            
        }
    }
    
    //MARK:- check email format function
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }

    
    //MARK:-  server call of login method
    func callcontactusMethod()
    {
        let message = txtf_comment.text!
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            
        }
        let apiCall = JsonApi()
        let parameters = [
            
            "user_id": UserDefaults.standard.object(forKey:"id")!,
            "email": txtf_email.text!,
            "name": txtf_username.text!,
            "message": message,
           
        ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalfeedbackUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            let status = result["status"] as! String
            if status != ""
            {
                let success = result["success"] as! String
                let message = result["message"] as! String
                
                print(status)
                if success == "true"
                {
                    // call alert controller in main queue
                    DispatchQueue.main.async {
                        self.activityIndicatorView.stopAnimating()
                        let alertView = UIAlertController(title: "Message", message: message, preferredStyle: .alert)
                        let addAction = UIAlertAction(title: "Done",style:UIAlertActionStyle.default, handler: {(alert :UIAlertAction!) in
                            // handle your code here
                            self.navigationController?.popViewController(animated: true)
                        })
                        alertView.addAction(addAction)
                        self.present(alertView, animated: true, completion: nil)
                    }
                }
                else
                {
                    // call alert controller in main queue
                    DispatchQueue.main.async {
                        self.activityIndicatorView.stopAnimating()
                        self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:message)
                    }
                }
                
            }
            else
            {
                DispatchQueue.main.async {
                    self.activityIndicatorView.stopAnimating()
                    self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:"Server Error! Please try again")
                }
                
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
