//
//  BlogViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 4/13/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class BlogViewController: UIViewController ,UIWebViewDelegate {
    @IBOutlet weak var webview: UIWebView!
   
    var activityIndicatorView: ActivityIndicatorView!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "BLOG"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
          webview.backgroundColor = UIColor.white
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            
        }
      
        let detail_url = "http://notmymothertongue.com"
        webview.scrollView.bounces = false
        let url = NSURL (string: detail_url)
        let requestObj = NSURLRequest(url: url! as URL);
        webview.loadRequest(requestObj as URLRequest)
        
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        DispatchQueue.main.async {
            self.activityIndicatorView.stopAnimating()
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
