//
//  CareerPlanningViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 4/4/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit
import Kingfisher

class CareerPlanningViewController: UIViewController {

  
      @IBOutlet var btn_careerPlanner: UIButton!
    @IBOutlet var btn_checkList: UIButton!
        override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "CAREER PLANNING"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
    }

    override func viewDidLoad() {
        super.viewDidLoad()

      
         btn_careerPlanner.layer.cornerRadius = btn_careerPlanner.frame.size.height/2
        btn_checkList.layer.cornerRadius = btn_checkList.frame.size.height/2

       // self.callgetCareerListMethod()
    }
    
    @IBAction func btn_careerPlannerAction(_ sender: Any) {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"CareerPlannerViewController") as! CareerPlannerViewController
        self.navigationItem.title = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_checkListAction(_ sender: Any) {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"CareerCheckListViewController") as! CareerCheckListViewController
        self.navigationItem.title = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // scrollView.contentSize = contentView.frame.size
//        if  self.view.frame.size.height > 570
//        {
//            scrollview.contentSize = CGSize(width:self.view.frame.size.width,height:self.view.frame.size.height+800)
//        }
//        else
//        {
//            scrollview.contentSize = CGSize(width:self.view.frame.size.width,height:self.view.frame.size.height+250)
//        }
    }
    
    
    // MARK:- Server call of getService Provider Method
 /*   func callgetCareerListMethod()
    {
        
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            // UIApplication.shared.beginIgnoringInteractionEvents()
        }
        let string_url = "https://api-v2.themuse.com/posts?page=" + String(paging)
        
        let url = URL(string: string_url)
        
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            guard let data = data else {
                print("Data is empty")
                return
            }
            
           // let json = try! JSONSerialization.jsonObject(with: data, options: [])
             let json = try! JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject]
            
            print(json)
            // parse data from server
            let careerArr = json?["results"] as! NSArray
            if careerArr.count != 0
            {
                self.parseGetCareerListData(dataArray: careerArr)
            }
            else
            {
                DispatchQueue.main.async {
                    self.activityIndicatorView.stopAnimating()
                    // UIApplication.shared.beginIgnoringInteractionEvents()
                }
            }
        }
        
        task.resume()
    }
    
    
    //  MARK:- parseGetserviceProviderListData
    func parseGetCareerListData(dataArray:NSArray)
    {
      
      
        let careerDict = NSMutableDictionary()
         let arrJson = dataArray as! Array<Dictionary<String,Any>>
        for i in 0...dataArray.count-1
        {
            
             let Object = arrJson[i] as [String : AnyObject]
            
            careerDict.setValue((Object["name"]), forKey: "name")
            careerDict.setValue((Object["excerpt"] as? NSString), forKey: "excerpt")
            let refs = Object["refs"] as! NSDictionary
            
            careerDict.setValue((refs["landing_page"] as? NSString), forKey: "landing_page")
            careerDict.setValue((refs["primary_image"] as? NSString), forKey: "primary_image")
            careerArr.add(careerDict .mutableCopy())
            
        }
        DispatchQueue.main.async
            {
                self.activityIndicatorView.stopAnimating()
                self.tbl_career.reloadData()
                
        }
    }

    //MARK:- tableview delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return careerArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(
            withIdentifier: "cell",
            for: indexPath)as! CareerPlanningTVCell
        
        
         let dict = careerArr[indexPath.row] as? Dictionary<String,String>
        //  cell.backgroundColor = UIColor.white
        
        cell.lbl_title.text = dict?["name"]
        cell.lbl_subtitle.text = dict?["excerpt"]
       // cell.image_view.image = UIImage.init(named: (dict?["primary_image"])!)
        let imageString =  dict?["primary_image"]
       
       
        
        DispatchQueue.main.async {
            let imgURL = URL(string:imageString!)
            cell.image_view.kf.setImage(with: imgURL)}

        
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 160
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let dict = careerArr[indexPath.row] as? Dictionary<String,String>
        let url = (dict?["landing_page"])! as String
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"CareerdetailViewController") as! CareerdetailViewController
        self.navigationItem.title = ""
        vc.detail_url = url
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if(indexPath.row == (self.careerArr.count-1)) {
            // if isMoreDataAvailable {
            paging = paging + 1
            self.callgetCareerListMethod()
            // }
            
            
        }
        
        
    }
    */

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
