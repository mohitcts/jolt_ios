//
//  ResourcesTVCell.swift
//  Jolt
//
//  Created by chawtech solutions on 4/13/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class ResourcesTVCell: UITableViewCell {
    @IBOutlet weak var lbl_resourcename: UILabel!
    @IBOutlet weak var lbl_resourceamount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
