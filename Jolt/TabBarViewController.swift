//
//  TabBarViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 3/16/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {
let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()

       navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.black]
         navigationController?.navigationBar.barTintColor = UIColor.white
        
        let notifBack = appDelegate.notifBack
        if notifBack == true
        {
            let data = appDelegate.UserInfo
            
            //let data = notification.userInfo?["data"]
            print(data)
            self.callMethodForPushNotification(data: data as! [String : AnyObject])
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = true

        //Register to receive notification in your class
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlepushnotification(_:)), name: NSNotification.Name(rawValue: "notificationFire"), object: nil)
        
        NotificationCenter.default.addObserver(
            self,selector: #selector(registerFcmTokentoServer),
            name:NSNotification.Name(rawValue: "REGISTERFCMTOKEN"),object: nil)
        
    }
    
    //MARK:- viewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(
            self,
            name:NSNotification.Name(rawValue:"REGISTERFCMTOKEN"),object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue:"notificationFire"), object: nil)
    }
    
    
    // handle notification
    func handlepushnotification(_ notification: NSNotification) {
        
        let data = notification.userInfo?["data"] as! [String:AnyObject]
        print(data)
        self.callMethodForPushNotification(data: data)
        
    }

    func callMethodForPushNotification(data:[String:AnyObject])
    {
        
        let notificationType = String(describing:data["notification_type"]!)
        let notification_type = notificationType.replacingOccurrences(of:"\"", with:"")
        
        if  notification_type == "Business Checklist"
        {
            DispatchQueue.main.async {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier:"BusinessCheckListViewController") as! BusinessCheckListViewController
                self.navigationController?.pushViewController(vc, animated: false)
            }

        }
        
        if  notification_type == "Business Checklist Quarter"
        {
            DispatchQueue.main.async {
                let alert: UIAlertController = UIAlertController(title: "Are you on track or behind your goal?", message: "", preferredStyle: .alert)
                let behindAction = UIAlertAction(title: "Behind",style:UIAlertActionStyle.default, handler: {(alert :UIAlertAction!) in
                    // handle your code here
                    
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let vc  = storyboard.instantiateViewController(withIdentifier:"ResourcesViewController") as! ResourcesViewController
                    self.navigationController?.pushViewController(vc, animated: false)
                    
                })
                let cancelAction = UIAlertAction(title: "Cancel",style:UIAlertActionStyle.default, handler: {(alert :UIAlertAction!) in
                    // handle your code here
                    
                })
                alert.addAction(behindAction)
                alert.addAction(cancelAction)
                self.present(alert, animated: true, completion: nil)
            
            }
            
        }
            
        else if  notification_type == "New Employee Checklist"
        {
            DispatchQueue.main.async {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier:"NewEmployeeViewController") as! NewEmployeeViewController
                self.navigationController?.pushViewController(vc, animated: false)
            }
            
        }
        else if  notification_type == "Current Employee Checklist"
        {
            DispatchQueue.main.async {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier:"CurrentEmployeeViewController") as! CurrentEmployeeViewController
                self.navigationController?.pushViewController(vc, animated: false)
            }
            
        }
        else if  notification_type == "Leader Checklist"
        {
            DispatchQueue.main.async {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier:"LeaderViewController") as! LeaderViewController
                self.navigationController?.pushViewController(vc, animated: false)
            }
            
        }
        else if  notification_type == "Employment Checklist"
        {
            DispatchQueue.main.async {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier:"EmploymentTransitionViewController") as! EmploymentTransitionViewController
                self.navigationController?.pushViewController(vc, animated: false)
            }
            
        }
    }
    
    func registerFcmTokentoServer(notification: NSNotification)
    {
        if let userInfo = notification.object as? [String:AnyObject] {
            let refreshToken = userInfo["refreshtoken"] as? String
            if refreshToken != nil {
                callupdateFcmTokenToServerMethod(refreshFcmToken:refreshToken!)
            }
            
        }
    }
    
    //MARK:-  server call of callupdateFcmTokenToServer Method
    func callupdateFcmTokenToServerMethod(refreshFcmToken:String)
    {
        let apiCall = JsonApi()
        let parameters = [
            
            "user_id": UserDefaults.standard.object(forKey:
                "id")!,
            "device":"ios",
            "fcm_key": refreshFcmToken,
            
            ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalupdateFcmTokensUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            print(result)
            
           
            let success = result["success"] as! String
            let message = result["message"] as! String
            
            
            if success == "true"
            {
            }
            else
            {
                // call alert controller in main queue
                DispatchQueue.main.async {
                    self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:message)
                }
            }
            
        }
        
    }

    
    // credit card as 4242-4242-4242-4242, cvv 123 12/12
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
