//
//  CoachTextField.swift
//  Jolt
//
//  Created by chawtech solutions on 4/5/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class CoachTextField: UITextField {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 5.0, dy:2.0)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return self.textRect(forBounds: bounds)
    }
}
