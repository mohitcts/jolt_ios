//
//  BusinessCheckListTVCell.swift
//  Jolt
//
//  Created by chawtech solutions on 4/10/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class BusinessCheckListTVCell: UITableViewCell {

    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var imgv_checkbox: UIImageView!
     @IBOutlet weak var btn_checkbox: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
