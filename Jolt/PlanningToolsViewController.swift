//
//  PlanningToolsViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 4/5/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class PlanningToolsViewController: UIViewController {

    @IBOutlet weak var btn_StartNow: UIButton!
    @IBOutlet weak var txtV_text: UITextView!
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        self.title = "INSTRUCTIONS"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        navigationController?.navigationBar.backItem?.title = ""
        btn_StartNow.layer.cornerRadius = btn_StartNow.frame.size.height/2

        
        txtV_text.text = "Setting career goals can be likened to creating a flight plan for a pilot. It defines the destination and the path to the destination. A goal helps a new employee define his/her destination. It helps answer questions such as:\n1. Do I plan to build and grow a career until retirement or plan to quit and start my own business?\n2. How long until I retire? Until I go independent?\n3. Where do I expect to end up in the social pyramid or professional circle?\n4. How long do I plan to reach my expected echelon in the corporate world?\n\nHowever, goals not anchored to a passion/ideal version of yourself can often change, leading to frequently changing jobs.\nTherefore before you define your career goals, start by defining yourself.Ask yourself the following questions:\n1. Who am I?/Who do I want become?\n2. What are my strengths while acknowledging that I have weaknesses?\n3. What is my passion?"
    }
    
    
    
    @IBAction func btn_nextAction(_ sender: Any)
    {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"OrganiserViewController") as! OrganiserViewController
        self.navigationItem.title = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
