//
//  CareerCheckListViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 4/11/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class CareerCheckListViewController: UIViewController {

    
    
    @IBOutlet var btn_newEmp: UIButton!
    @IBOutlet var btn_currentEmp: UIButton!
    
    @IBOutlet weak var btn_setreminder: UIButton!
    @IBOutlet var btn_leader: UIButton!
    @IBOutlet var btn_empTransition: UIButton!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "CAREER CHECKLIST"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        btn_newEmp.layer.cornerRadius = btn_newEmp.frame.size.height/2
        btn_currentEmp.layer.cornerRadius = btn_currentEmp.frame.size.height/2

        btn_leader.layer.cornerRadius = btn_leader.frame.size.height/2
        btn_empTransition.layer.cornerRadius = btn_empTransition.frame.size.height/2
       btn_setreminder.layer.cornerRadius = btn_setreminder.frame.size.height/2
    }

    @IBAction func btn_newEmpAction(_ sender: Any) {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"NewEmployeeViewController") as! NewEmployeeViewController
        self.navigationItem.title = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_currentEmpAction(_ sender: Any) {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"CurrentEmployeeViewController") as! CurrentEmployeeViewController
        self.navigationItem.title = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func btn_leaderAction(_ sender: Any) {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"LeaderViewController") as! LeaderViewController
        self.navigationItem.title = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_emptransitionAction(_ sender: Any) {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"EmploymentTransitionViewController") as! EmploymentTransitionViewController
        self.navigationItem.title = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func btn_setreminder(_ sender: Any) {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"SetReminderViewController") as! SetReminderViewController
        self.navigationItem.title = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
