//
//  DashboardViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 3/16/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
      @IBOutlet weak var lbl_username: UILabel!
    @IBOutlet weak var collectionview: UICollectionView!
     @IBOutlet weak var view_main: UIView!
     var collectionTitle_array = ["COACHING","JOB BOARD","PLANNING TOOLS","ASSESSMENT TEST"]
     var collectionImages_array = ["coach", "job", "organiserplanner","assisment-test"]
    
    
    @IBAction func btn_feedbsckAction(_ sender: Any)
    {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"FeedbackViewController") as! FeedbackViewController
        self.navigationItem.title = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
       override func viewWillAppear(_ animated: Bool)
       {
           super.viewWillAppear(animated)
        
            self.title = "DASHBOARD"
            navigationController?.navigationBar.isTranslucent = false
            self.navigationController?.isNavigationBarHidden = false
    
       }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
        if let name = UserDefaults.standard.object(forKey: "name") as? String
        {
            self.lbl_username.text = name
        }
    }
    
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.collectionTitle_array.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! DashboardCVCell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        cell.lbl_title.text = self.collectionTitle_array[indexPath.item]
         cell.image_view.image = UIImage.init(named: self.collectionImages_array[indexPath.item])
       // cell.backgroundColor = UIColor.cyan // make cell more visible in our example project
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        
        if indexPath.item == 0
        {
            let storyboard = UIStoryboard(name:"Main", bundle: nil)
            let vc  = storyboard.instantiateViewController(withIdentifier:"CoachForUsViewController") as! CoachForUsViewController
            self.navigationItem.title = ""
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.item == 1
        {
            let storyboard = UIStoryboard(name:"Main", bundle: nil)
            let vc  = storyboard.instantiateViewController(withIdentifier:"JobBoardViewController") as! JobBoardViewController
            self.navigationItem.title = ""
            self.navigationController?.pushViewController(vc, animated: true)

        }
        if indexPath.item == 2
        {
            let storyboard = UIStoryboard(name:"Main", bundle: nil)
            let vc  = storyboard.instantiateViewController(withIdentifier:"PlanningToolsViewController") as! PlanningToolsViewController
            self.navigationItem.title = ""
            self.navigationController?.pushViewController(vc, animated: true)

                   }
        if indexPath.item == 3
        {
        
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"AssessmentTestViewController") as! AssessmentTestViewController
        self.navigationItem.title = ""
        self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      
        var screenWidth: CGFloat!
        var screenHeight: CGFloat!
        screenWidth = view_main.frame.size.width
        screenHeight = view_main.frame.size.height
        return CGSize(width: screenWidth/2-5, height: screenHeight/2)
        
    }
    
    @IBAction func btn_coachForUsAction(_ sender: Any) {
       
       
    }
    
    
    @IBAction func btn_assessmentTestAction(_ sender: Any) {
        
    }
    
    
    @IBAction func btn_jobBoardAction(_ sender: Any) {
            }
    
    
    @IBAction func btn_organiserAction(_ sender: Any) {
            }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
