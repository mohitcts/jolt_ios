//
//  PersonalityViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 3/24/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class PersonalityViewController: UIViewController {

     @IBOutlet var lbl_per_style: UILabel!
     @IBOutlet var lbl_personality: UILabel!
     @IBOutlet var lbl_social: UILabel!
     @IBOutlet var lbl_work_enviornment: UILabel!
     @IBOutlet var lbl_do: UILabel!
      @IBOutlet var lbl_dont: UILabel!
    
    
    var peronalityInfo = NSDictionary()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "PERSONALITY"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        lbl_per_style.text = peronalityInfo["personality_style"] as? String
        lbl_personality.text = peronalityInfo["personality"] as? String
        lbl_social.text = peronalityInfo["social"] as? String
        lbl_work_enviornment.text = peronalityInfo["work_environment"] as? String
        lbl_do.text = peronalityInfo["do_interacting"] as? String
         lbl_dont.text = peronalityInfo["do_not_interacting"] as? String
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
