//
//  OrganiserViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 3/17/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class OrganiserViewController: UIViewController {
    @IBOutlet var btn_career: UIButton!
    @IBOutlet var btn_businessDev: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "PLANNING TOOLS"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btn_career.layer.cornerRadius = btn_career.frame.size.height/2
        btn_businessDev.layer.cornerRadius = btn_businessDev.frame.size.height/2
    }
    @IBAction func btn_careerAction(_ sender: Any) {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"CareerPlanningViewController") as! CareerPlanningViewController
         self.navigationItem.title = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_businessDevAction(_ sender: Any) {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"BusinessDevelopmentViewController") as! BusinessDevelopmentViewController
        self.navigationItem.title = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
