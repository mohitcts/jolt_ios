//
//  MiniBusinessPdfViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 4/22/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class MiniBusinessPdfViewController: UIViewController ,UIWebViewDelegate {
    
    
    @IBOutlet weak var webview: UIWebView!
    var detail_url = NSURL()
    var title_name = String()
    
    var activityIndicatorView: ActivityIndicatorView!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = title_name
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
                let share_btn = UIButton(type: .custom)
                share_btn.setImage(UIImage(named: "share"), for: .normal)
                share_btn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
                share_btn.addTarget(self, action: #selector(MiniBusinessPdfViewController.clickOnShareMethod), for: .touchUpInside)
                let share_btnn = UIBarButtonItem(customView: share_btn)
        
                self.navigationItem.setRightBarButtonItems([share_btnn], animated: true)

        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            
        }
//        let documentsUrl:URL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL!
//        let destinationFileurl = documentsUrl.appendingPathComponent("businessplan.pdf")
//        print(destinationFileurl)

        webview.scrollView.bounces = true
        //let url = NSURL (string: detail_url)
        let requestObj = NSURLRequest(url: detail_url as URL)
        webview.loadRequest(requestObj as URLRequest)
        
    }
    

    func clickOnShareMethod(sender: UIBarButtonItem)
    {
        let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [detail_url], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView=self.view
        present(activityViewController, animated: true, completion: nil)
        
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {
        DispatchQueue.main.async {
            self.activityIndicatorView.stopAnimating()
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
