//
//  CarrerViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 3/21/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class CarrerViewController: UIViewController {

        @IBOutlet var btn_next: UIButton!
        @IBOutlet var btn_specific_career: UIButton!
        @IBOutlet var txtf_ideal_self: UITextField!
        @IBOutlet var txtf_ideal_attributes: UITextField!
        @IBOutlet var txtf_trainning_needs: UITextField!
        @IBOutlet var txtf_ideal_strength: UITextField!
        @IBOutlet var txtf_career_path: UITextField!
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "CARRER PLANNING"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewDidLoad() {
        super.viewDidLoad()

       self.setRadiousofTextfield()
    }
    
func setRadiousofTextfield()
{
    btn_specific_career.layer.cornerRadius = btn_specific_career.frame.size.height/2
    txtf_ideal_self.layer.cornerRadius = txtf_ideal_self.frame.size.height/2
    txtf_ideal_self.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)

    txtf_ideal_attributes.layer.cornerRadius = txtf_ideal_attributes.frame.size.height/2
    txtf_ideal_attributes.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
    txtf_trainning_needs.layer.cornerRadius = txtf_trainning_needs.frame.size.height/2
    txtf_trainning_needs.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)

    txtf_ideal_strength.layer.cornerRadius = txtf_ideal_strength.frame.size.height/2
    txtf_ideal_strength.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)

    txtf_career_path.layer.cornerRadius = txtf_career_path.frame.size.height/2
    txtf_career_path.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)

    btn_next.layer.cornerRadius = btn_next.frame.size.height/2
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
