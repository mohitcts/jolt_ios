//
//  FindRecruiterViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 4/21/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class FindRecruiterViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    var activityIndicatorView: ActivityIndicatorView!
    @IBOutlet weak var tbl_recruiterList: UITableView!
 
    var  recruitersListArr = NSMutableArray()
 
    var  paging = Int()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "RECRUITERS LIST"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
        
       
    }

    override func viewDidLoad() {
        super.viewDidLoad()

         tbl_recruiterList.tableHeaderView = UIView(frame: CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: CGFloat(0.0), height: CGFloat(0.2)))
        self.callgetRecruitersListMethod()
    }
    // MARK:- Server call of getService Provider Method
    func callgetRecruitersListMethod()
    {
        
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            // UIApplication.shared.beginIgnoringInteractionEvents()
        }
        let string_url = kbaseUrl + kadditionalrecruiterListUrl
        
        let url = URL(string: string_url)
        
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            let dataString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            print(dataString as Any)

            // let json = try! JSONSerialization.jsonObject(with: data, options: [])
            let json = try! JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject]
            
             print(json as Any)
            // parse data from server
            let careerArr = json?["data"] as! NSArray
            if careerArr.count != 0
            {
                self.parseGetCareerListData(dataArray: careerArr)
            }
            else
            {
                DispatchQueue.main.async {
                    self.activityIndicatorView.stopAnimating()
                    // UIApplication.shared.beginIgnoringInteractionEvents()
                }
            }
        }
        
        task.resume()
    }
    
    
    //  MARK:- parseGetserviceProviderListData
    func parseGetCareerListData(dataArray:NSArray)
    {
        recruitersListArr.removeAllObjects()
        let recruitDict = NSMutableDictionary()
        let arrJson = dataArray as! Array<Dictionary<String,Any>>
        for i in 0...dataArray.count-1
        {
            
            let Object = arrJson[i] as [String : AnyObject]
            
            recruitDict.setValue((Object["id"]), forKey: "id")
           
            
            recruitDict.setValue((Object["company"] ), forKey: "company")
            recruitDict.setValue((Object["address"] ), forKey: "address")
            recruitDict.setValue((Object["website"] ), forKey: "website")
            recruitDict.setValue((Object["telephone"] ), forKey: "telephone")
            recruitDict.setValue((Object["fax"] ), forKey: "fax")
            recruitDict.setValue((Object["service_offered"] ), forKey: "service_offered")
            recruitersListArr.add(recruitDict .mutableCopy())
           
            
        }
        DispatchQueue.main.async
            {
                self.activityIndicatorView.stopAnimating()
                self.tbl_recruiterList.reloadData()
                
        }
    }
    
    //MARK:- tableview delegate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return recruitersListArr.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(
            withIdentifier: "cell",
            for: indexPath)as! RecruiterTVCell
        
        
        let dict = recruitersListArr[indexPath.section] as? Dictionary<String,String>
        //  cell.backgroundColor = UIColor.white
        
        cell.lbl_company.text = dict?["company"]
          cell.lbl_address.text = dict?["address"]
          cell.lbl_telephone.text = dict?["telephone"]
          cell.lbl_fax.text = dict?["fax"]
        cell.lbl_website.text = dict?["website"]
        cell.lbl_service.text = dict?["service_offered"]
        
        
        return cell
        
    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
//    {
//        return 217
//    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
       return 2
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
//        let dict = recruitersListArr[indexPath.row] as? Dictionary<String,String>
//        let url = (dict?["landing_page"])! as String
//        let storyboard = UIStoryboard(name:"Main", bundle: nil)
//        let vc  = storyboard.instantiateViewController(withIdentifier:"JobBoardDetailViewController") as! JobBoardDetailViewController
//        self.navigationItem.title = ""
//        vc.detail_url = url
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if(indexPath.row == (self.recruitersListArr.count-1)) {
//            // if isMoreDataAvailable {
//            paging = paging + 1
//            self.callgetRecruitersListMethod()
//            // }
//            
//            
//        }
//        
//        
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
