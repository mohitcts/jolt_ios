//
//  RecruiterTVCell.swift
//  Jolt
//
//  Created by chawtech solutions on 4/21/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class RecruiterTVCell: UITableViewCell {
    @IBOutlet weak var lbl_company: UILabel!
    @IBOutlet weak var lbl_address: UILabel!
    @IBOutlet weak var lbl_telephone: UILabel!
    @IBOutlet weak var lbl_fax: UILabel!
    @IBOutlet weak var lbl_website: UILabel!
    @IBOutlet weak var lbl_service: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
