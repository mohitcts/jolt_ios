//
//  ForgetpasswordViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 3/20/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class ForgetpasswordViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var txtf_email: CustomTextField!
    var activityIndicatorView: ActivityIndicatorView!

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
      
        
        // TODO:- set textfield placeholder
        txtf_email.attributedPlaceholder = NSAttributedString(string:"Please Enter Email", attributes: [NSForegroundColorAttributeName: UIColor.init(red: 121/255.0, green: 132/255.0, blue: 140/255.0, alpha: 1)])
        txtf_email.layer.borderWidth = 1
        txtf_email.layer.borderColor = UIColor.lightGray.cgColor
        
        self.txtf_email.bs_setupErrorMessageView(withMessage:"Please Enter Email")
        
        //TODO:-  set tap gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.removeKeyboard(_:)))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    //MARK:- remove Keyboard action of tapgesture
    func removeKeyboard(_ sender: UITapGestureRecognizer) {
        
        self.view.endEditing(true)
    }

    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        textField.bs_hideError()
    }
    
    @IBAction func btn_resetAction(_ sender: Any) {
        
        if ((txtf_email.text?.characters.count)! > 0)
        {
            if isValidEmail(testStr: txtf_email.text!)  == true
            {
                self.callForgetPasswordMethod()
            }
            else
            {
                self.txtf_email.bs_setupErrorMessageView(withMessage:"Please Enter Valid  Email")
                self.txtf_email.bs_showError()
            }
        }
        else
        {
            self.txtf_email.bs_showError()
        }
    }
    
    //MARK:- check email format function
        func isValidEmail(testStr:String) -> Bool {
            // print("validate calendar: \(testStr)")
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
            
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            return emailTest.evaluate(with: testStr)
        }
    
    @IBAction func btn_cancelAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    
    //MARK:-  server call of forget password method
    func callForgetPasswordMethod()
    {
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            
        }
        let apiCall = JsonApi()
        let parameters = [
            
            "email": txtf_email.text!,
           
        ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalforgetPasswordUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            let status = result["status"] as! String
            if status != ""
            {
                let success = result["success"] as! String
                let message = result["message"] as! String
                
                print(status)
                if success == "true"
                {
                   
                    // call alert controller in main queue
                    DispatchQueue.main.async {
                        self.activityIndicatorView.stopAnimating()
                        self.alertController(message: message)
                    }
                }
                else
                {
                    // call alert controller in main queue
                    DispatchQueue.main.async {
                        self.activityIndicatorView.stopAnimating()
                        self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:message)
                    }
                }
                
            }
            else
            {
                
            }
        }
    }

    func alertController(message:String)
    {
        // create the alert
        let alert = UIAlertController(title:"Message", message:message, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            DispatchQueue.main.async {
                self.navigationController?.popViewController(animated: false)
            }
            
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
        
    

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
