//
//  AssessmentTestViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 3/17/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class AssessmentTestViewController: UIViewController {
 @IBOutlet weak var btn_StartNow: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "INSTRUCTIONS"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
    }

    override func viewDidLoad() {
        super.viewDidLoad()
      
        navigationController?.navigationBar.backItem?.title = ""
        btn_StartNow.layer.cornerRadius = btn_StartNow.frame.size.height/2
    }
    @IBAction func btn_nextAction(_ sender: Any) {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"QuestionnaireViewController") as! QuestionnaireViewController
         self.navigationItem.title = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
