//
//  CareerPlannerViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 4/11/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class CareerPlannerViewController: UIViewController {

     var activityIndicatorView: ActivityIndicatorView!
     @IBOutlet var btn_submit: UIButton!
        @IBOutlet var txtv_idealself: UITextView!
        @IBOutlet var txtv_idealattribute: UITextView!
        @IBOutlet var txtv_trainningneeds: UITextView!
        @IBOutlet var txtv_goals2years: UITextView!
        @IBOutlet var txtv_goals5years: UITextView!
        @IBOutlet var txtv_goals10years: UITextView!
        @IBOutlet var txtv_idealstrength: UITextView!
        @IBOutlet var txtv_devopportunities: UITextView!
        @IBOutlet var txtv_otherdevtools: UITextView!
     @IBOutlet var txtv_careerpath: UITextView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "CAREER PLANNER"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewDidLoad() {
        super.viewDidLoad()

          btn_submit.layer.cornerRadius = btn_submit.frame.size.height/2
    }

    
     @IBAction func btn_submitAction(_ sender: Any) {
        
        if (txtv_idealself.text.characters.count > 0)
        && (txtv_careerpath.text.characters.count > 0)
        && (txtv_goals2years.text.characters.count > 0)
        && (txtv_goals5years.text.characters.count > 0)
        && (txtv_goals10years.text.characters.count > 0)
        && (txtv_idealstrength.text.characters.count > 0)
        && (txtv_otherdevtools.text.characters.count > 0)
        && (txtv_trainningneeds.text.characters.count > 0)
        && (txtv_idealattribute.text.characters.count > 0)
        && (txtv_devopportunities.text.characters.count > 0)
        {
              self.callCareerPlannerMethod()
        }
        else
        {
            self.displayAlertWithMessage(viewController: self, title: "Alert!", message: "Please Fill All Detail")
        }

      
    }
    
    
    //MARK:-  server call of login method
    func callCareerPlannerMethod()
    {
        
        
     let goals2years =  txtv_goals2years.text!
        let goals5years =  txtv_goals5years.text!
          let goals10years =  txtv_goals10years.text!
          let idealstrength =  txtv_idealstrength.text!
          let careerpath =  txtv_careerpath.text!
          let devopportunities =  txtv_devopportunities.text!
          let otherdevtools =  txtv_otherdevtools.text!
          let idealself =  txtv_idealself.text!
          let idealattribute =  txtv_idealattribute.text!
        let trainningneeds =  txtv_trainningneeds.text!
        
        
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            
        }
        let apiCall = JsonApi()
        let parameters = [
            
            "goals_for_2years": goals2years,
            "goals_for_5years": goals5years,
            "goals_for_10years": goals10years,
            "ideal_strengths": idealstrength,
            "career_path": careerpath,
            "development_opportunities": devopportunities,
            "other_development_needs":otherdevtools,
            "ideal_self": idealself,
            "ideal_attribute": idealattribute,
            "training_needs": trainningneeds,
            "user_id": UserDefaults.standard.object(forKey: "id")!
        ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionaladdcareerPlanUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            let status = result["status"] as! String
            if status != ""
            {
                let success = result["success"] as! String
                let message = result["message"] as! String
                
                print(status)
                if success == "true"
                {
                    // set all key values in user defaults
                    let data = result["data"] as! NSDictionary
                    let pdfUrl = data["pdfurl"] as! String
                    self.downloadPdfFile(pdf_path: pdfUrl)
                }
                else
                {
                    // call alert controller in main queue
                    DispatchQueue.main.async {
                        self.activityIndicatorView.stopAnimating()
                        self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:message)
                    }
                }
                
            }
            else
            {
                DispatchQueue.main.async {
                    self.activityIndicatorView.stopAnimating()
                    self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:"Server Error! Please try again")
                }
                
            }
        }
    }
    
    func downloadPdfFile(pdf_path: String)
    {
        
        
        // Create destination URL
        let documentsUrl:URL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL!
        let destinationFileUrl = documentsUrl.appendingPathComponent("careerplan.pdf")
        print(destinationFileUrl)
        
        if FileManager().fileExists(atPath: destinationFileUrl.path) {
            print("The file already exists at path")
            do {
                try FileManager.default.removeItem(at:destinationFileUrl)
            }catch let error as NSError {
                print(error.debugDescription)
            }
        }
        
        
        
        //Create URL to the source file you want to download
        // let fileURL = URL(string: "35.167.21.236/joltapi/businessplans/plan_pdf/2")
        let fileURL = URL(string: pdf_path)
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        
        let request = URLRequest(url:fileURL!)
        
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {
                // Success
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    print("Successfully downloaded. Status code: \(statusCode)")
                    
                    DispatchQueue.main.async {
                        
                        self.activityIndicatorView.stopAnimating()
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let vc  = storyboard.instantiateViewController(withIdentifier:"MiniBusinessPdfViewController") as! MiniBusinessPdfViewController
                        self.navigationItem.title = ""
                         vc.title_name = "CAREER PLANNER"
                        vc.detail_url = destinationFileUrl as NSURL
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                
                do {
                    try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                } catch (let writeError) {
                    print("Error creating a file \(destinationFileUrl) : \(writeError)")
                }
                
            } else {
                print("Error took place while downloading a file. Error description: %@", error?.localizedDescription as Any)
            }
        }
        task.resume()
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
