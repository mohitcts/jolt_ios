//
//  StoreDetailViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 4/26/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class StoreDetailViewController: UIViewController ,UIWebViewDelegate{
 var activityIndicatorView: ActivityIndicatorView!
    @IBOutlet weak var webview: UIWebView!
    var resourceTitle = String()
    var resourceName = String()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = resourceTitle
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
        
       
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let share_btn = UIButton(type: .custom)
        share_btn.setImage(UIImage(named: "share"), for: .normal)
        //share_btn.setTitle("BUY", for: .normal)
        share_btn.setTitleColor(UIColor.init(red: 0/255.0, green: 122/255.0, blue: 255/255.0, alpha: 1), for: .normal)
        share_btn.frame = CGRect(x: 0, y: 0, width: 50, height: 30)
        share_btn.addTarget(self, action: #selector(StoreDetailViewController.clickOnshareMethod), for: .touchUpInside)
        let share_btnn = UIBarButtonItem(customView: share_btn)
        
        self.navigationItem.setRightBarButtonItems([share_btnn], animated: true)
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            
        }

        
        webview.delegate = self
        if let pdf = Bundle.main.url(forResource: resourceName, withExtension: "pdf", subdirectory: nil, localization: nil)  {
            let req = NSURLRequest(url: pdf)
            webview.loadRequest(req as URLRequest)
            self.view.addSubview(webview)
        }
    }
    
    
        func webViewDidFinishLoad(_ webView: UIWebView) {
            DispatchQueue.main.async {
                self.activityIndicatorView.stopAnimating()
                
            }
        }

        
    func clickOnshareMethod(sender: UIBarButtonItem)
    {
        let filePathToUpload = URL(fileURLWithPath: Bundle.main.path(forResource: resourceName, ofType: "pdf")!)
        let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [filePathToUpload], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView=self.view
        present(activityViewController, animated: true, completion: nil)
        

    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
