//
//  SearchViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 4/18/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController ,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate {
    
     var string_url = String()
    var activityIndicatorView: ActivityIndicatorView!
     @IBOutlet var btn_search: UIButton!
        @IBOutlet var txtf_search: UITextField!
    @IBOutlet var txtf_filterJobs: UITextField!
    @IBOutlet var txtf_location: UITextField!
    let pickerDataArr =  ["Account Management","Creative & Design","Data Science","Education","Finance","Healthcare & Medicine","Legal","Operations","Retail","Social Media & Community","Business & Strategy","Customer Service","Editorial","Engineering","Fundraising & Development","HR & Recruiting","Marketing & PR","Project & Product Management","Sales"]
    var paging = Int()
     var  jobBoardArr = NSMutableArray()
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "SEARCH JOBS"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         txtf_search.layer.sublayerTransform = CATransform3DMakeTranslation(5, 2, 2)
         txtf_location.layer.sublayerTransform = CATransform3DMakeTranslation(5, 2, 2)
         txtf_filterJobs.layer.sublayerTransform = CATransform3DMakeTranslation(5, 2, 2)
        btn_search.layer.cornerRadius = btn_search.frame.size.height/2
        txtf_filterJobs.delegate = self
        txtf_search.delegate = self
        txtf_filterJobs.text = pickerDataArr[0]
        paging = 0
       
    }
    
    @IBAction func btn_searchAction(_ sender: Any) {
       self.callgetJobFilterListMethod()
        
    }
    
    
    
    // MARK:- Server call of getService Provider Method
    func callgetJobFilterListMethod()
    {
        
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            // UIApplication.shared.beginIgnoringInteractionEvents()
        }
        
      //  https://api-v2.themuse.com/jobs?search=Fremantle&category=Account+Management&location=Accra&page=1
        
        
        if ((txtf_location.text?.characters.count)! > 0) && ((txtf_filterJobs.text?.characters.count)! > 0) && ((txtf_location.text?.characters.count)! > 0)
        {
              string_url = "https://api-v2.themuse.com/jobs?search=" + txtf_search.text! + "&category=" + txtf_filterJobs.text! + "&location=" + txtf_location.text! + "&page=" + String(paging)
        }
      else if ((txtf_location.text?.characters.count)! == 0)
        {
            string_url = "https://api-v2.themuse.com/jobs?search=" + txtf_search.text! + "&category=" + txtf_filterJobs.text! + "&page=" + String(paging)
        }
        else if ((txtf_search.text?.characters.count)! == 0)
        {
           string_url = "https://api-v2.themuse.com/jobs?&category=" + txtf_filterJobs.text! + "&location=" + txtf_location.text! + "&page=" + String(paging)
        }
        
        
       // let url = URL(string:string_url)
         let url = URL(string: string_url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            guard let data = data else {
                print("Data is empty")
                return
            }
            
            // let json = try! JSONSerialization.jsonObject(with: data, options: [])
            let json = try! JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject]
            
            print(json as Any)
            // parse data from server
            let careerArr = json?["results"] as! NSArray
            if careerArr.count != 0
            {
                self.parseGetCareerListData(dataArray: careerArr)
            }
            else
            {
                DispatchQueue.main.async {
                    self.activityIndicatorView.stopAnimating()
                   self.displayAlertWithMessage(viewController: self, title: "Alert!", message: "No Result Found")
                }
            }
        }
        
        task.resume()
    }
    
    
    //  MARK:- parseGetserviceProviderListData
    func parseGetCareerListData(dataArray:NSArray)
    {
        jobBoardArr.removeAllObjects()
        let careerDict = NSMutableDictionary()
        let arrJson = dataArray as! Array<Dictionary<String,Any>>
        for i in 0...dataArray.count-1
        {
            
            let Object = arrJson[i] as [String : AnyObject]
            
            careerDict.setValue((Object["name"]), forKey: "name")
            // careerDict.setValue((Object["contents"] as? NSString), forKey: "contents")
            let company = Object["company"] as! NSDictionary
            let locations = Object["locations"] as! NSArray
           
            if locations.count != 0
            {
                let obj = locations[0] as! [String : AnyObject]
                careerDict.setValue((obj["name"] as? NSString), forKey: "location_name")
            }
            else
            {
                careerDict.setValue("Location Not Available", forKey: "location_name")
            }
            let refs = Object["refs"] as! NSDictionary
            
            
            careerDict.setValue((company["name"] as? NSString), forKey: "company_name")
            careerDict.setValue((refs["landing_page"] as? NSString), forKey: "landing_page")
            careerDict.setValue((refs["primary_image"] as? NSString), forKey: "primary_image")
            jobBoardArr.add(careerDict .mutableCopy())
           
            
        }
        DispatchQueue.main.async
            {
               
                let strArr = self.string_url.components(separatedBy: "0")
                
                let stringUrlArr = NSMutableArray()
                stringUrlArr.add(strArr[0])
                self.activityIndicatorView.stopAnimating()
              _ =  self.navigationController?.popViewController(animated: true)
                let imageDataDict:[String: NSMutableArray] = ["jobarray": self.jobBoardArr,"url": stringUrlArr]
                
                // post a notification
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "jobfilterprovider"), object: nil, userInfo: imageDataDict)
             
            }
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.createPickerview()
        textField.bs_hideError()
    }
    
    
    func createPickerview()
    {
        var picker = UIPickerView()
        picker = UIPickerView(frame: CGRect(x:0,y: 200, width:view.frame.width,height:200))
        picker.backgroundColor = .white
        picker.showsSelectionIndicator = true
        picker.delegate = self
        picker.dataSource = self
      
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black//UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(FindCoachViewController.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
       // let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(FindCoachViewController.donePicker))
        
        toolBar.setItems([ spaceButton,doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        txtf_filterJobs.inputView = picker
        txtf_filterJobs.inputAccessoryView = toolBar
       
        for i in 0...pickerDataArr.count-1
        {
        if  txtf_filterJobs.text == pickerDataArr[i]
        {
              picker.selectRow(i, inComponent: 0, animated: true)
        }
        }
    }
    
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataArr.count
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
         return pickerDataArr[row]
       
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
         txtf_filterJobs.text = pickerDataArr[row]
        
    }
    
    func donePicker() {
        
        txtf_filterJobs.resignFirstResponder()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
