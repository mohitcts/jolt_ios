//
//  ViewController.swift
//  Jolt
//
//  Created by ChawtechSolutions on 22/02/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
 var database = FMDatabase()
      var dataPath = String()
    //MARK:- viewDidLoad()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // TODO:- set navigation controller properties
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationBar.isTranslucent = false
        
        
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentsDirectory:AnyObject = paths[0] as AnyObject
        dataPath = documentsDirectory.appendingPathComponent(dataBaseName)
        
        self.CreateSqliteDatabase()

        
        // TODO:- wait splash for 2 min after that call next controller
        let when = DispatchTime.now() + 2 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            
            
            let email = UserDefaults.standard.object(forKey:"email")as? String
            
            if email == nil
            {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                self.navigationController?.pushViewController(vc, animated: false)
            }
            else
            {
                let storyboard = UIStoryboard(name:"Main", bundle: nil)
                let vc  = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
                 vc.selectedIndex = 1
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }
    }
    func CreateSqliteDatabase() {
        
        if !(FileManager.default.fileExists(atPath: dataPath as String))
        {
            database = FMDatabase(path:dataPath as String)
            database .open()
            
            //  DB for category data
            // (database executeUpdate:"CREATE TABLE IF NOT EXISTS DEVICE_TABLE (ID INTEGER PRIMARY KEY AUTOINCREMENT,DEVICEID TEXT NOT NULL,DEVICENAME TEXT NOT NULL,GROUPNAME TEXT NOT NULL)")
            
            database.executeStatements("CREATE TABLE IF NOT EXISTS ASSISMENT_TABLE (ID INTEGER PRIMARY KEY AUTOINCREMENT,QUESTIONID TEXT NOT NULL,FIRSTQUESTION TEXT NOT NULL,SECONDQUESTION TEXT NOT NULL,CATEGORY TEXT NOT NULL,PAGE TEXT NOT NULL,ANSWER TEXT NOT NULL)")
            
            // database.executeStatements("CREATE TABLE IF NOT EXISTS DEVICE_TABLE (ID INTEGER PRIMARY KEY AUTOINCREMENT,DEVICEID TEXT NOT NULL,DEVICENAME TEXT NOT NULL,GROUPNAME TEXT NOT NULL,DEVICESTATUS TEXT NOT NULL,DEVICENUMBER TEXT NOT NULL,DEVICESSID TEXT NOT NULL,DEVICETYPE TEXT NOT NULL)")
        }
        
        database.close()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

