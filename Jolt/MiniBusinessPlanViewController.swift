//
//  MiniBusinessPlanViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 3/24/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class MiniBusinessPlanViewController: UIViewController,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{

    @IBOutlet var btn_next: UIButton!
   
     @IBOutlet var txtf_date: UITextField!
    @IBOutlet var txtf_actionplan1: UITextField!
     @IBOutlet var txtf_actionplan2: UITextField!
     @IBOutlet var txtf_actionplan3: UITextField!
     @IBOutlet var txtf_actionplan4: UITextField!
     @IBOutlet var txtf_actionplan5: UITextField!
     @IBOutlet var txtf_businessreq1: UITextField!
     @IBOutlet var txtf_businessreq2: UITextField!
     @IBOutlet var txtf_businessreq3: UITextField!
     @IBOutlet var txtf_businessreq4: UITextField!
     @IBOutlet var txtf_businessreq5: UITextField!
    
    @IBOutlet var txtf_company_objectives1: UITextField!
    @IBOutlet var txtf_company_objectives2: UITextField!
    @IBOutlet var txtf_company_objectives3: UITextField!
    @IBOutlet var txtf_company_objectives4: UITextField!
    @IBOutlet var txtf_company_objectives5: UITextField!
    @IBOutlet var txtf_company_goals1: UITextField!
    @IBOutlet var txtf_company_goals2: UITextField!
    @IBOutlet var txtf_company_goals3: UITextField!
    @IBOutlet var txtf_company_goals4: UITextField!
    @IBOutlet var txtf_company_goals5: UITextField!
    
     @IBOutlet var txtf_companyName: UITextField!
     @IBOutlet var txtf_companyMotto: UITextField!
     @IBOutlet var txtf_comapnyMission: UITextField!
     @IBOutlet var txtf_marketAnalysis: UITextField!
    var picker = UIPickerView()
    var toolBar = UIToolbar()
    var selectValue = Bool()
    var activityIndicatorView: ActivityIndicatorView!
    var chosenImage = UIImage()
     let pickerDataArr = ["1","2","3","4","5"]
    // Create a DatePicker
    let datePicker: UIDatePicker = UIDatePicker()
    var selectedDate = String()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "MINI BUSINESS PLAN"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
        self.setplaceholderTextfield()
    }
    
    func setplaceholderTextfield()
    {
        // TODO:- set textfield placeholder
        txtf_date.attributedPlaceholder = NSAttributedString(string:"DATE", attributes: [NSForegroundColorAttributeName: UIColor.black])
        
        txtf_companyName.attributedPlaceholder = NSAttributedString(string:"COMPANY NAME", attributes: [NSForegroundColorAttributeName: UIColor.black])
        // TODO:- set textfield placeholder
        txtf_companyMotto.attributedPlaceholder = NSAttributedString(string:"COMPANY MOTTO", attributes: [NSForegroundColorAttributeName: UIColor.black])
        
        txtf_comapnyMission.attributedPlaceholder = NSAttributedString(string:"COMPANY MISSION STATEMENT", attributes: [NSForegroundColorAttributeName: UIColor.black])
        // TODO:- set textfield placeholder
        txtf_marketAnalysis.attributedPlaceholder = NSAttributedString(string:"MARKET ANALYSIS", attributes: [NSForegroundColorAttributeName: UIColor.black])
        
        
        
        btn_next.layer.cornerRadius = btn_next.frame.size.height/2

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let result = formatter.string(from: date)
        txtf_date .text = result
        txtf_date.delegate = self
        
//        let storyboard = UIStoryboard(name:"Main", bundle: nil)
//        let vc  = storyboard.instantiateViewController(withIdentifier:"DetailMiniBusinessPlanViewController") as! DetailMiniBusinessPlanViewController
//        self.navigationItem.title = ""
//        // Create destination URL
//        let documentsUrl:URL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL!
//        let destinationFileUrl = documentsUrl.appendingPathComponent("businessplan.pdf")
//        print(destinationFileUrl)
//        vc.detail_url = destinationFileUrl as URL
//    
//        self.navigationController?.pushViewController(vc, animated: true)
        
       
        
        // Posiiton date picket within a view
        datePicker.frame = CGRect(x: 10, y: 50, width: self.view.frame.width, height: 200)
        
        // Set some of UIDatePicker properties
       // datePicker.timeZone = NSTimeZone.local
        datePicker.datePickerMode = UIDatePickerMode.date
        datePicker.backgroundColor = UIColor.white
        
        // Add an event to call onDidChangeDate function when value is changed.
        datePicker.addTarget(self, action: #selector(MiniBusinessPlanViewController.datePickerValueChanged(_:)), for: .valueChanged)
        // Add DataPicker to the view
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black//UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
         toolBar.backgroundColor = UIColor.black
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(FindCoachViewController.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        // let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(FindCoachViewController.donePicker))
        
        //  toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        
        toolBar.isUserInteractionEnabled = true

      //  self.view.addSubview(datePicker)
        txtf_date.inputView = datePicker
        txtf_date.inputAccessoryView = toolBar
        datePicker.isHidden = true
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtf_date
        {
             datePicker.isHidden = false
        }
       
    }
   
    func donePicker() {
        
       txtf_date.resignFirstResponder()
         datePicker.isHidden = true
      
        
    }
func datePickerValueChanged(_ sender: UIDatePicker){
    
    // Create date formatter
    let dateFormatter: DateFormatter = DateFormatter()
    
    // Set date format
    dateFormatter.dateFormat = "yyyy-MM-dd"
    
    // Apply date format
    selectedDate = dateFormatter.string(from: sender.date)
    
    print("Selected value \(selectedDate)")
     txtf_date.text = selectedDate
}
    /*
    func createPickerview()
    {
 
        picker = UIPickerView(frame: CGRect(x:0,y: 200, width:view.frame.width,height:200))
        picker.backgroundColor = .white
        
        picker.showsSelectionIndicator = true
        picker.delegate = self
        picker.dataSource = self
        
       
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black//UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(FindCoachViewController.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
       // let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(FindCoachViewController.donePicker))
        
      //  toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.setItems([spaceButton, doneButton], animated: false)

        toolBar.isUserInteractionEnabled = true
        
       
    }
    
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataArr.count
    }
    
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
         return pickerDataArr[row]
       // let dict = pickerDataArr[row] as? Dictionary<String,String>
       // return(dict?["category_name"])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if selectValue == false
        {
               txtf_company_objectives.text = pickerDataArr[row]
        }
        else if selectValue == true
        {
               txtf_company_goals.text = pickerDataArr[row]
        }
      
        // txtf_category.text = pickerDataArr[row]["category_name"]
       // let dict = pickerDataArr[row] as? Dictionary<String,String>
       // txtf_category.text = dict?["category_name"]
    }
    
    func donePicker() {
        
        txtf_company_goals.resignFirstResponder()
        txtf_company_objectives.resignFirstResponder()
        
    }
    */
    
    //MARK:- btn upload Vehicle Image action
    @IBAction func btn_uploadProfileImage(_ sender: Any)
    {
        let alert = UIAlertController(title:"Pick Any One", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Camera", style: .default) { action in
            // perhaps use action.title here
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                imagePicker.allowsEditing = false
                imagePicker.cameraCaptureMode = .photo
                imagePicker.modalPresentationStyle = .fullScreen
                self.present(imagePicker, animated: true, completion: nil)
            }
        })
        alert.addAction(UIAlertAction(title: "Photos", style: .default) { action in
            // perhaps use action.title here
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
            
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .default) { action in
            // perhaps use action.title here
        })
        
        self.present(alert, animated: true)
    }
    //MARK:- imagePickerController delegate methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        //  myImageView.contentMode = .scaleAspectFit //3
        // myImageView.image = chosenImage //4
       
        dismiss(animated:true, completion: nil) //5
    }
    
    
    func downloadPdfFile(pdf_path: String)
    {
      
        
        // Create destination URL
        let documentsUrl:URL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL!
        let destinationFileUrl = documentsUrl.appendingPathComponent("businessplan.pdf")
        print(destinationFileUrl)
        
        if FileManager().fileExists(atPath: destinationFileUrl.path) {
            print("The file already exists at path")
            do {
                try FileManager.default.removeItem(at:destinationFileUrl)
            }catch let error as NSError {
                print(error.debugDescription)
            }
        }

        
        
        //Create URL to the source file you want to download
       // let fileURL = URL(string: "35.167.21.236/joltapi/businessplans/plan_pdf/2")
        let fileURL = URL(string: pdf_path)
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        
        let request = URLRequest(url:fileURL!)
        
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {
                // Success
                if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                    print("Successfully downloaded. Status code: \(statusCode)")
                    
                    DispatchQueue.main.async {
                        
                    self.activityIndicatorView.stopAnimating()
                    let storyboard = UIStoryboard(name:"Main", bundle: nil)
                    let vc  = storyboard.instantiateViewController(withIdentifier:"MiniBusinessPdfViewController") as! MiniBusinessPdfViewController
                       self.navigationItem.title = ""
                        vc.title_name = "MINI BUSINESS PLAN"
                    // Create destination URL
//                    let documentsUrl:URL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL!
//                    let destinationFileurl = documentsUrl.appendingPathComponent("businessplan.pdf")
//                    print(destinationFileUrl)
                    vc.detail_url = destinationFileUrl as NSURL
                   self.navigationController?.pushViewController(vc, animated: true)
                    }
                    }
                
                do {
                    try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                } catch (let writeError) {
                    print("Error creating a file \(destinationFileUrl) : \(writeError)")
                }
                
            } else {
                print("Error took place while downloading a file. Error description: %@", error?.localizedDescription as Any)
            }
        }
        task.resume()
        
    }

      
        
        
//            //The URL to Save
//            let yourURL = NSURL(string: "http://somewebsite.com/somefile.pdf")
//            //Create a URL request
//            let urlRequest = NSURLRequest(url: yourURL! as URL)
//            //get the data
//            let theData = NSURLConnection.sendSynchronousRequest(urlRequest, returningResponse: nil, error: nil)
//            
//            //Get the local docs directory and append your local filename.
//            var docURL = (FileManager.defaultManager.URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)).last as? NSURL
//            
//            docURL = docURL?.URLByAppendingPathComponent( "myFileName.pdf")
//            
//            //Lastly, write your file to the disk.
//            theData?.writeToURL(docURL!, atomically: true)
        
    
     @IBAction func btn_nextAction(_ sender: Any)
     {
        
        
        self.callsubmitMiniPlanMethod()
        
       

        
    }
    func generateBoundaryString() -> String
    {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    
    
    func callsubmitMiniPlanMethod()
    {
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        
        DispatchQueue.main.async
            {
                self.activityIndicatorView.startAnimating()
        }
        
        let postPictureUrl = NSURL(string: kbaseUrl + kadditionalminibusinessPlanUrl)
        let request = NSMutableURLRequest(url: postPictureUrl! as URL)
        request.httpMethod="POST"
        
        var jsonString = String()
        let jsonObject: NSMutableDictionary = NSMutableDictionary()
        
        jsonObject.setValue(UserDefaults.standard.object(forKey:"id"), forKey:"user_id")
        jsonObject.setValue(txtf_date.text, forKey: "plan_date")
        jsonObject.setValue(txtf_companyName.text, forKey: "company_name")
        jsonObject.setValue(txtf_companyMotto.text, forKey: "company_moto")
        jsonObject.setValue(txtf_comapnyMission.text, forKey: "mission_statement")
        jsonObject.setValue(txtf_marketAnalysis.text!, forKey: "market_analysis")
         jsonObject.setValue(txtf_actionplan1.text!, forKey: "action_plan1")
         jsonObject.setValue(txtf_actionplan2.text!, forKey: "action_plan2")
        jsonObject.setValue(txtf_actionplan3.text!, forKey: "action_plan3")
        jsonObject.setValue(txtf_actionplan4.text!, forKey: "action_plan4")
         jsonObject.setValue(txtf_actionplan5.text!, forKey: "action_plan5")
        jsonObject.setValue(txtf_businessreq1.text!, forKey: "requirement_plan1")
        jsonObject.setValue(txtf_businessreq2.text!, forKey: "requirement_plan2")
        jsonObject.setValue(txtf_businessreq3.text!, forKey: "requirement_plan3")
        jsonObject.setValue(txtf_businessreq4.text!, forKey: "requirement_plan4")
        jsonObject.setValue(txtf_businessreq5.text!, forKey: "requirement_plan5")
        jsonObject.setValue(txtf_company_objectives1.text!, forKey: "objective1")
        jsonObject.setValue(txtf_company_objectives2.text!, forKey: "objective2")
        jsonObject.setValue(txtf_company_objectives3.text!, forKey: "objective3")
        jsonObject.setValue(txtf_company_objectives4.text!, forKey: "objective4")
        jsonObject.setValue(txtf_company_objectives5.text!, forKey: "objective5")
        jsonObject.setValue(txtf_company_goals1.text!, forKey: "goals1")
        jsonObject.setValue(txtf_company_goals2.text!, forKey: "goals2")
        jsonObject.setValue(txtf_company_goals3.text!, forKey: "goals3")
        jsonObject.setValue(txtf_company_goals4.text!, forKey: "goals4")
        jsonObject.setValue(txtf_company_goals5.text!, forKey: "goals5")


        let jsonData: NSData
        
        do {
            jsonData = try JSONSerialization.data(withJSONObject: jsonObject, options: JSONSerialization.WritingOptions()) as NSData
            jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String
            print("json string = \(jsonString)")
            
            
        } catch _ {
            print ("JSON Failure")
        }
        
        
        let param = [
            
            "data": jsonString
        ]
        
        //  let abc =  KeychainWrapper.stringForKey("tokenValue")!
        
        let boundary = generateBoundaryString()
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        
        var imageData = UIImageJPEGRepresentation(chosenImage, 0.1)
        
        if imageData==nil
        {
           // imageData = NSData() as Data
            let image = UIImage.init(named:"no-image.png")
            imageData =  UIImageJPEGRepresentation(image!, 0.1)
            print("image data is nil")
        }
        
        request.httpBody = createBodyWithParameters(parameters:param, filePathKey: "logo", imageDataKey: imageData! as NSData, boundary: boundary) as Data
        
        
       //  request.httpBody = createBodyWithParameters(parameters:param, filePathKey: "logo", imageDataKey:, boundary: boundary) as Data
        
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print("error=\(String(describing: error))")
                return
            }
            
            // Print out reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("**** response data = \(responseString!)")
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: AnyObject] {
                    print(json)
                    let status = json["success"] as! String
                    let message = json["message"]as! String
                    
                    if status == "true"
                    {
                        // set all key values in user defaults
                        let data = json["data"] as! NSDictionary
                        let pdfUrl = data["pdfurl"] as! String
                        self.downloadPdfFile(pdf_path: pdfUrl)
                        
//                           DispatchQueue.main.async {
//                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
//                        let vc  = storyboard.instantiateViewController(withIdentifier:"MiniBusinessPdfViewController") as! MiniBusinessPdfViewController
//                        self.navigationItem.title = ""
//                        // Create destination URL
//                        //                    let documentsUrl:URL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first as URL!
//                        //                    let destinationFileurl = documentsUrl.appendingPathComponent("businessplan.pdf")
//                        //                    print(destinationFileUrl)
//                        //                    vc.detail_url = destinationFileurl
//                        self.navigationController?.pushViewController(vc, animated: true)
                      //  }
                       
                    }
                    else
                    {
                        // call alert controller in main queue
                        DispatchQueue.main.async {
                            self.activityIndicatorView.stopAnimating()
                            self.displayAlertWithMessage(viewController:self, title:"Message!" , message:message)
                        }
                    }
                    
                    
                }
            } catch let err {
                print(err)
            }
        }
        task.resume()
    }
    
    //    func generateBoundaryString() -> String {
    //        return "Boundary-\(NSUUID().uuidString)"
    //    }
    
    
    func createBodyWithParameters(parameters:[String:Any]?, filePathKey: String?, imageDataKey:NSData, boundary: String) -> NSData {
        
        let body=NSMutableData()
        
        if parameters != nil {
            for(key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        let filename = "user-profile.jpg"
        let mimetype = "image/jpg"
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

