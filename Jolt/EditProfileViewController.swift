//
//  EditProfileViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 3/29/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    @IBOutlet weak var txtf_mobile: UITextField!
  var activityIndicatorView: ActivityIndicatorView!
    @IBOutlet weak var txtf_age: UITextField!
    @IBOutlet weak var txtf_degree: UITextField!
    @IBOutlet weak var txtf_email: UITextField!
    @IBOutlet weak var txtf_name: UITextField!
      @IBOutlet weak var txtf_city: UITextField!
     @IBOutlet weak var txtf_password: UITextField!
     @IBOutlet weak var txtf_confirmpassword: UITextField!
    @IBOutlet weak var scrollview: UIScrollView!
      @IBOutlet weak var imgV_profile: UIImageView!
     
     var chosenImage = UIImage()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "EDIT PROFILE"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       // scrollView.contentSize = contentView.frame.size
//        if  self.view.frame.size.height > 570
//        {
//            scrollview.contentSize = CGSize(width:self.view.frame.size.width,height:self.view.frame.size.height+200)
//        }
//        else
//        {
//            scrollview.contentSize = CGSize(width:self.view.frame.size.width,height:self.view.frame.size.height+250)
//        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imgV_profile.layer.cornerRadius = imgV_profile.frame.size.height/2
        imgV_profile.clipsToBounds = true
        if let name = UserDefaults.standard.object(forKey: "name") as? String
        {
            self.txtf_name.text = name
        }
        if let email = UserDefaults.standard.object(forKey: "email") as? String
        {
            self.txtf_email.text = email
        }
        if let city = UserDefaults.standard.object(forKey: "city") as? String
        {
            self.txtf_city.text = city
        }
        if let mobile = UserDefaults.standard.object(forKey: "mobile") as? String
        {
            self.txtf_mobile.text = mobile
        }
        if let graduation = UserDefaults.standard.object(forKey: "graduation") as? String
        {
            self.txtf_degree.text = graduation
        }
        
        if let age_in_year = UserDefaults.standard.object(forKey: "age_in_year") as? String
        {
            self.txtf_age.text = age_in_year
        }
        
       // txtf_name.text = UserDefaults.standard.object(forKey: "name") as! String?
       // txtf_email.text = UserDefaults.standard.object(forKey: "email") as! String?
       // txtf_name.text = UserDefaults.standard.object(forKey: "name") as? String?
        
        self.txtf_name.bs_setupErrorMessageView(withMessage: "Please Enter Name")
        self.txtf_password.bs_setupErrorMessageView(withMessage: "Please Enter password")
        self.txtf_mobile.bs_setupErrorMessageView(withMessage: "Please Enter Mobile")
        self.txtf_age.bs_setupErrorMessageView(withMessage: "Please Enter Age")
        self.txtf_degree.bs_setupErrorMessageView(withMessage: "Please Enter Graduation")
         self.txtf_city.bs_setupErrorMessageView(withMessage: "Please Enter City")

    }

    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        textField.bs_hideError()
    }
    
    
    //MARK:- btn signup Action
    @IBAction func btn_submitAction(_ sender: Any)
    {
        
        if ((txtf_name.text?.characters.count)! > 0)
        {
            if ((txtf_mobile.text?.characters.count)! > 0)
            {
                if ((txtf_city.text?.characters.count)! > 0)
                {
                if ((txtf_degree.text?.characters.count)! > 0)
                {
                    if ((txtf_age.text?.characters.count)! > 0)
                    {
                    if ((txtf_password.text?.characters.count)! > 0)
                    {
                        if  txtf_password.text == txtf_confirmpassword.text
                        {
                            
                                self.callupdtaeProfileMethod()
                        }
                        else
                        {
                            self.txtf_confirmpassword.bs_setupErrorMessageView(withMessage: "Password Does Not match")
                            txtf_confirmpassword.bs_showError()
                        }
                        
                    }
                    else
                    {
                        //self.txtf_password.bs_showError()
                        self.callupdtaeProfileMethod()
                        txtf_password.text = ""
                    }
                }
                else
                {
                        self.txtf_age.bs_showError()
                        
                }
                }
                else
                {
                    self.txtf_degree.bs_showError()
                    
                }
            }
                else
                {
                    self.txtf_city.bs_showError()
                    
                }
            } else
            {
                self.txtf_mobile.bs_showError()
                
            }
                
            }
            else
            {
                self.txtf_name.bs_showError()
            }
            
        
    }
    //MARK:- btn upload Vehicle Image action
    @IBAction func btn_uploadProfileImage(_ sender: Any)
    {
        let alert = UIAlertController(title:"Pick Any One", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Camera", style: .default) { action in
            // perhaps use action.title here
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                imagePicker.allowsEditing = false
                imagePicker.cameraCaptureMode = .photo
                imagePicker.modalPresentationStyle = .fullScreen
                self.present(imagePicker, animated: true, completion: nil)
            }
        })
        alert.addAction(UIAlertAction(title: "Photos", style: .default) { action in
            // perhaps use action.title here
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
            
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .default) { action in
            // perhaps use action.title here
        })
        
        self.present(alert, animated: true)
    }
    //MARK:- imagePickerController delegate methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        //  myImageView.contentMode = .scaleAspectFit //3
        // myImageView.image = chosenImage //4
        imgV_profile.image = chosenImage
        dismiss(animated:true, completion: nil) //5
    }

    
    func generateBoundaryString() -> String
    {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    
    
    func callupdtaeProfileMethod()
    {
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        
        DispatchQueue.main.async
        {
              self.activityIndicatorView.startAnimating()
        }
        
        let postPictureUrl = NSURL(string: kbaseUrl + kadditionaleditProfileUrl)
        let request = NSMutableURLRequest(url: postPictureUrl! as URL)
        request.httpMethod="POST"
        
        var jsonString = String()
        let jsonObject: NSMutableDictionary = NSMutableDictionary()
        
        jsonObject.setValue(UserDefaults.standard.object(forKey:"id"), forKey:"user_id")
        jsonObject.setValue(txtf_name.text, forKey: "name")
        jsonObject.setValue(txtf_email.text, forKey: "email")
        jsonObject.setValue(txtf_mobile.text, forKey: "mobile")
        jsonObject.setValue(txtf_degree.text, forKey: "graduation")
        jsonObject.setValue(txtf_age.text!, forKey: "age_in_year")
        jsonObject.setValue(txtf_city.text!, forKey: "city")
        jsonObject.setValue(txtf_password.text!, forKey: "password")
       
        
        let jsonData: NSData
        
        do {
            jsonData = try JSONSerialization.data(withJSONObject: jsonObject, options: JSONSerialization.WritingOptions()) as NSData
            jsonString = NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String
            print("json string = \(jsonString)")
            
            
        } catch _ {
            print ("JSON Failure")
        }
        
    
        let param = [
            
            "data": jsonString
        ]
        
        //  let abc =  KeychainWrapper.stringForKey("tokenValue")!
        
        let boundary = generateBoundaryString()
        
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        var imageData = UIImageJPEGRepresentation(chosenImage, 0.1)
        
        if imageData==nil
        {
            imageData = NSData() as Data
            print("image data is nil")
        }
        
        request.httpBody = createBodyWithParameters(parameters:param, filePathKey: "photo", imageDataKey: imageData! as NSData, boundary: boundary) as Data
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            
            if error != nil {
                print("error=\(String(describing: error))")
                return
            }
            
            // Print out reponse body
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            print("**** response data = \(responseString!)")
            
            do {
                
                if let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: AnyObject] {
                    print(json)
                    let status = json["success"] as! String
                    let message = json["message"]as! String
                    
                    if status == "true"
                    {
                        // call alert controller in main queue
                        DispatchQueue.main.async {
                            self.activityIndicatorView.stopAnimating()
                            let alert = UIAlertController(title: "Message", message: message, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default) { action in
                                _ = self.navigationController?.popViewController(animated: true)
                            })
                            self.present(alert, animated: true)
                            
                        }
                        
                    }
                    else
                    {
                        // call alert controller in main queue
                        DispatchQueue.main.async {
                            self.activityIndicatorView.stopAnimating()
                            self.displayAlertWithMessage(viewController:self, title:"Message!" , message:message)
                        }
                    }
                    
                    
                }
            } catch let err {
                print(err)
            }
        }
        task.resume()
    }
    
    //    func generateBoundaryString() -> String {
    //        return "Boundary-\(NSUUID().uuidString)"
    //    }
    
    
    func createBodyWithParameters(parameters:[String:Any]?, filePathKey: String?, imageDataKey:NSData, boundary: String) -> NSData {
        
        let body=NSMutableData()
        
        if parameters != nil {
            for(key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        let filename = "user-profile.jpg"
        let mimetype = "image/jpg"
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    

    //MARK:-  server call of login method
    func callupdtaeProfileMethod1()
    {
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            
        }
        let apiCall = JsonApi()
        let parameters = [
            "age_in_year": txtf_age.text!,
            "graduation": txtf_degree.text!,
            "mobile": txtf_mobile.text!,
            "name": txtf_name.text!,
            "email": txtf_email.text!,
            "password": txtf_password.text!,
            "user_id": UserDefaults.standard.object(forKey: "id")!,
            
        ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalLoginUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            let status = result["status"] as! String
            if status != ""
            {
                let success = result["success"] as! String
                let message = result["message"] as! String
                
                print(status)
                if success == "true"
                {
                    // set all key values in user defaults
                    let data = result["data"] as! NSDictionary
                    UserDefaults.standard.setValue(data["email"], forKey: "email")
                    
                    
                    UserDefaults.standard.setValue(data["name"], forKey: "name")
                    
                    UserDefaults.standard.setValue(data["id"], forKey: "id")
                    UserDefaults.standard.setValue(data["registerDate"], forKey: "registerDate")
                    UserDefaults.standard.setValue(data["password"], forKey: "password")
                    // call next view controller in main queue
                    DispatchQueue.main.async {
                        self.activityIndicatorView.stopAnimating()
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let vc  = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
                        vc.selectedIndex = 1
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                }
                else
                {
                    // call alert controller in main queue
                    DispatchQueue.main.async {
                        self.activityIndicatorView.stopAnimating()
                        self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:message)
                    }
                }
                
            }
            else
            {
                DispatchQueue.main.async {
                    self.activityIndicatorView.stopAnimating()
                    self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:"Server Error! Please try again")
                }
                
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension NSMutableData {
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
