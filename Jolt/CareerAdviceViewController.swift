//
//  CareerAdviceViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 4/10/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit
import Kingfisher

class CareerAdviceViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{


    var activityIndicatorView: ActivityIndicatorView!
      @IBOutlet weak var tbl_career: UITableView!
    var  careerArr = NSMutableArray()
    var  paging = Int()
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "CAREER ADVICE"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        paging = 0
      // tbl_career.tableHeaderView = nil
        tbl_career.tableHeaderView = UIView(frame: CGRect(x: CGFloat(0.0), y: CGFloat(0.0), width: CGFloat(0.0), height: CGFloat(0.2)))
        self.callgetCareerAdviceListMethod()
    }
    
    
    // MARK:- Server call of getService Provider Method
       func callgetCareerAdviceListMethod()
     {
     
     activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
     self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
     DispatchQueue.main.async {
     self.activityIndicatorView.startAnimating()
     // UIApplication.shared.beginIgnoringInteractionEvents()
     }
     let string_url = "https://api-v2.themuse.com/posts?page=" + String(paging)
     
     let url = URL(string: string_url)
     
     let task = URLSession.shared.dataTask(with: url!) { data, response, error in
     guard error == nil else {
     print(error!)
     return
     }
     guard let data = data else {
     print("Data is empty")
     return
     }
     
     // let json = try! JSONSerialization.jsonObject(with: data, options: [])
     let json = try! JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject]
     
     print(json as Any)
     // parse data from server
     let careerArr = json?["results"] as! NSArray
     if careerArr.count != 0
     {
     self.parseGetCareerListData(dataArray: careerArr)
     }
     else
     {
     DispatchQueue.main.async {
     self.activityIndicatorView.stopAnimating()
     // UIApplication.shared.beginIgnoringInteractionEvents()
     }
     }
     }
     
     task.resume()
     }
     
     
     //  MARK:- parseGetserviceProviderListData
     func parseGetCareerListData(dataArray:NSArray)
     {
     
     
     let careerDict = NSMutableDictionary()
     let arrJson = dataArray as! Array<Dictionary<String,Any>>
     for i in 0...dataArray.count-1
     {
     
     let Object = arrJson[i] as [String : AnyObject]
     
     careerDict.setValue((Object["name"]), forKey: "name")
     careerDict.setValue((Object["excerpt"] as? NSString), forKey: "excerpt")
     let refs = Object["refs"] as! NSDictionary
     
     careerDict.setValue((refs["landing_page"] as? NSString), forKey: "landing_page")
     careerDict.setValue((refs["primary_image"] as? NSString), forKey: "primary_image")
     careerArr.add(careerDict .mutableCopy())
     
     }
     DispatchQueue.main.async
     {
     self.activityIndicatorView.stopAnimating()
     self.tbl_career.reloadData()
     
     }
     }
     
     //MARK:- tableview delegate methods
     func numberOfSections(in tableView: UITableView) -> Int {
     return careerArr.count
     }
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     return 1
     }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
     let cell = tableView.dequeueReusableCell(
     withIdentifier: "cell",
     for: indexPath)as! CareerAdviceTVCell
     
     
     let dict = careerArr[indexPath.section] as? Dictionary<String,String>
     //  cell.backgroundColor = UIColor.white
     
     cell.lbl_title.text = dict?["name"]
     cell.lbl_subtitle.text = dict?["excerpt"]
     // cell.image_view.image = UIImage.init(named: (dict?["primary_image"])!)
     let imageString =  dict?["primary_image"]
     
     
     
     DispatchQueue.main.async {
     let imgURL = URL(string:imageString!)
     cell.image_view.kf.setImage(with: imgURL)}
     
     
     return cell
     
     }
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
     {
     return 130
     }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat
    {
        return 2
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
     {
     
     let dict = careerArr[indexPath.section] as? Dictionary<String,String>
     let url = (dict?["landing_page"])! as String
     let storyboard = UIStoryboard(name:"Main", bundle: nil)
     let vc  = storyboard.instantiateViewController(withIdentifier:"CareerAdviceDetailViewController") as! CareerAdviceDetailViewController
     self.navigationItem.title = ""
     vc.detail_url = url
     self.navigationController?.pushViewController(vc, animated: true)
     }
     
    
     func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
     if(indexPath.section == (self.careerArr.count-1)) {
     // if isMoreDataAvailable {
     paging = paging + 1
     self.callgetCareerAdviceListMethod()
     // }
        }
     }
 

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
