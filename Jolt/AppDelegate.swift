//
//  AppDelegate.swift
//  Jolt
//
//  Created by ChawtechSolutions on 22/02/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit
import CoreData
import FBSDKLoginKit
import GoogleSignIn
import IQKeyboardManager
import Firebase
import UserNotifications
import UserNotificationsUI


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,FIRMessagingDelegate  {
var tabBarController: UITabBarController?
    var window: UIWindow?

    var notifBack: Bool!
    
    var UserInfo = [AnyHashable: Any]()
    
    
    
    //for displaying notification when app is in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //If you don't want to show notification when app is open, do something here else and make a return here.
        //Even you you don't implement this delegate method, you will not see the notification on the specified controller. So, you have to implement this delegate and make sure the below line execute. i.e. completionHandler.
        
        completionHandler([.alert,.badge,.sound])
    }
    func callLoginControllerAfterLogout()
    {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"LoginViewController") as! LoginViewController
         let navC = UINavigationController(rootViewController: vc)
        self.window?.rootViewController = navC
        
    }
    
    
    func application(_ application: UIApplication,
                     open url: URL, options: [UIApplicationOpenURLOptionsKey: Any]) -> Bool{
     GIDSignIn.sharedInstance().handle(url,
    sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
    annotation: options[UIApplicationOpenURLOptionsKey.annotation] as? String)
        
        FBSDKApplicationDelegate.sharedInstance().application(application,
    open: url,sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String?,annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        
        
        return true
}
   
    
//    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
//        
//       // var options: [String: AnyObject] = [UIApplicationOpenURLOptionsKey.sourceApplication.rawValue: sourceApplication as AnyObject,
//        UIApplicationOpenURLOptionsKey.annotation.rawValue: annotation as AnyObject]
//        return GIDSignIn.sharedInstance().handle(url,
//        sourceApplication: sourceApplication,annotation: annotation)
//        
//        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
//    }
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
//        UINavigationBar.appearance().barTintColor = UIColor.init(red: 129/255.0, green: 180/255.0, blue: 65/255.0, alpha: 1)
//        UINavigationBar.appearance().tintColor = UIColor.white
//        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        
        PayPalMobile .initializeWithClientIds(forEnvironments: [PayPalEnvironmentProduction: "Achb2TS0qpvaRGaWawF0fa3urt0LlkfVoHpTa21QqZbL--PEaFgK10qsFaj86jppvdWPdZGT1YCPqAOV",
            PayPalEnvironmentSandbox: "SANDBOX CODE"])
        
        
        IQKeyboardManager.shared().isEnabled = true
        
        let barAppearace = UIBarButtonItem.appearance()
        barAppearace.setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        
         // Initialize sign-in facebook
         FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        
        //  Mark:- FCM Registration
        FIRApp.configure()
        
        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            
            
            // For iOS 10 data message (sent via FCM)
            FIRMessaging.messaging().remoteMessageDelegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        NotificationCenter.default.addObserver(self, selector:
            #selector(tokenRefreshNotification), name:
            NSNotification.Name.firInstanceIDTokenRefresh, object: nil)
        
        application.registerForRemoteNotifications()
        
        let token = FIRInstanceID.instanceID().token()
        print(token as Any)
        
        
        if launchOptions != nil {
            //opened from a push notification when the app is closed
            let userInfo: [AnyHashable: Any]? = (launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as?  [AnyHashable: Any])
            if userInfo != nil {
                
                notifBack = true
                UserInfo = userInfo!
            }
        }
        
        return true
    }
    // MARK: - Private Methods
    private func requestAuthorization(completionHandler: @escaping (_ success: Bool) -> ()) {
        // Request Authorization
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (success, error) in
            if let error = error {
                print("Request Authorization Failed (\(error), \(error.localizedDescription))")
            }
            
            completionHandler(success)
        }
    }
    func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage)
    {
        
    }
    
    
    // MARK:- token Refresh Notification method for FCM
    func tokenRefreshNotification(_ notification: Notification) {
        let refreshedToken = FIRInstanceID.instanceID().token()
        //            print("InstanceID token: \(refreshedToken)")
        //
        //
        //
        //            UserDefaults.standard.set(refreshedToken, forKey:"fcm_token")
        //            UserDefaults.standard.synchronize()
        //        }
        // Connect to FCM since connection may have failed when attempted before having a token.
        if (refreshedToken != nil)
        {
            
            let id = UserDefaults.standard.object(forKey:"id")as? String
            if id != nil {
                
                if refreshedToken != UserDefaults.standard.object(forKey:"fcm_token") as? String
                {
                    
                    let userinfo: [String:AnyObject] = ["refreshtoken": refreshedToken! as AnyObject]
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "REGISTERFCMTOKEN"), object:userinfo)
                    
                }
            }
            else
            {
               // UserDefaults.standard.setvalue(refreshedToken, forKey:"fcm_token")
                UserDefaults.standard.setValue(refreshedToken, forKey: "fcm_token")
                UserDefaults.standard.synchronize()
            }
        }
        print(refreshedToken as Any)
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    
    
    
    // MARK:- connectToFcm method for FCM
    func connectToFcm() {
        // Won't connect since there is no token
        guard FIRInstanceID.instanceID().token() != nil else {
            return;
        }
        
        // Disconnect previous FCM connection if it exists.
        FIRMessaging.messaging().disconnect()
        
        FIRMessaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(String(describing: error))")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
    }
    
    
    //    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
    //
    //    }
    
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        // Let FCM know about the message for analytics etc.
        FIRMessaging.messaging().appDidReceiveMessage(userInfo)
        // handle your message
    }
    
    
    // MARK:- didRegisterForRemoteNotificationsWithDeviceToken
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        FIRInstanceID.instanceID().setAPNSToken(deviceToken as Data, type: FIRInstanceIDAPNSTokenType.sandbox)
    }
    
    
    //MARK:- didReceiveRemoteNotification
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // Print message ID.
        //   if let messageID = userInfo[gcmMessageIDKey] {
        //      print("Message ID: \(messageID)")
        //  }
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // Print message ID.
        //  if let messageID = userInfo[gcmMessageIDKey] {
        //      print("Message ID: \(messageID)")
        //  }
        
        // Print full message.
        print(userInfo)
        // UNUserNotificationCenter.current().delegate = self
        // self.scheduleNotifications()
        //        UNUserNotificationCenter.current().getNotificationSettings { (notificationSettings) in
        //            switch notificationSettings.authorizationStatus {
        //            case .notDetermined:
        //                self.requestAuthorization(completionHandler: { (success) in
        //                    guard success else { return }
        //
        //                    // Schedule Local Notification
        //                    self.scheduleLocalNotification()
        //                })
        //            case .authorized:
        //                // Schedule Local Notification
        //                self.scheduleLocalNotification()
        //            case .denied:
        //                print("Application Not Allowed to Display Notifications")
        //            }
        //        }
        
        
        let DataDict:[String: AnyObject] = ["data": userInfo as AnyObject]
        
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue:"notificationFire"), object: nil, userInfo: DataDict)
        
        
      
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    //  There is a delegate method to display the notification when the app is open in iOS 10. You have to implement this in order to get the rich notifications working when the app is open.
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        FIRMessaging.messaging().disconnect()
        print("Disconnected from FCM.")
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Jolt")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

