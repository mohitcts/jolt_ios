//
//  CoachListViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 4/5/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class CoachListViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{

    
    var activityIndicatorView: ActivityIndicatorView!
    @IBOutlet weak var tbl_coachList: UITableView!
    @IBOutlet weak var txtf_search: UITextField!
    var  coachListArr = NSMutableArray()
    var  paging = Int()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "COACH LIST"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
    }

    override func viewDidLoad() {
        super.viewDidLoad()

       self.getCoachListFromServer()
        txtf_search.layer.borderColor = UIColor.lightGray.cgColor
        txtf_search.layer.borderWidth = 1
        txtf_search.layer.sublayerTransform = CATransform3DMakeTranslation(10, 0, 0)
        // TODO:- set textfield placeholder
        txtf_search.attributedPlaceholder = NSAttributedString(string:"Search", attributes: [NSForegroundColorAttributeName: UIColor.black])
    }
    
    
    
func getCoachListFromServer()
{
    activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
    self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
    DispatchQueue.main.async {
        self.activityIndicatorView.startAnimating()
        // UIApplication.shared.beginIgnoringInteractionEvents()
    }
    let string_url = kbaseUrl + kadditionalcoachListUrl
    
    let url = URL(string: string_url)
    
    let task = URLSession.shared.dataTask(with: url!) { data, response, error in
        guard error == nil else {
            print(error!)
            return
        }
        guard let data = data else {
            print("Data is empty")
            return
        }
        
        // let json = try! JSONSerialization.jsonObject(with: data, options: [])
        let json = try! JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: AnyObject]
        
        print(json as Any)
        // parse data from server
        let coachArr = json?["data"] as! NSArray
        //                self.parseGetserviceProviderListData(dataArray: venderArr!)
        if coachArr.count != 0
        {
            self.parseGetCoachListData(dataArray: coachArr)
        }
        else
        {
            DispatchQueue.main.async {
                self.activityIndicatorView.stopAnimating()
                // UIApplication.shared.beginIgnoringInteractionEvents()
            }
        }
    }
    
    task.resume()
}


//  MARK:- parseGetserviceProviderListData
func parseGetCoachListData(dataArray:NSArray)
{

   coachListArr.removeAllObjects()
    let careerDict = NSMutableDictionary()
    let arrJson = dataArray as! Array<Dictionary<String,Any>>
    for i in 0...dataArray.count-1
    {
        
        let Object = arrJson[i] as [String : AnyObject]
       let f_name =  Object["first_name"] as! String
        let l_name =  Object["last_name"] as! String
        let name = f_name + " " + l_name
        careerDict.setValue(name, forKey: "name")
        careerDict.setValue((Object["category_name"] as? NSString), forKey: "category_name")
       
        coachListArr.add(careerDict .mutableCopy())
        
    }
    DispatchQueue.main.async
        {
            self.activityIndicatorView.stopAnimating()
            self.tbl_coachList.reloadData()
            
    }
}

//MARK:- tableview delegate methods
func numberOfSections(in tableView: UITableView) -> Int {
    return 1
}
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return coachListArr.count
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(
        withIdentifier: "cell",
        for: indexPath)as! CoachListTVCell
    
    
    let dict = coachListArr[indexPath.row] as? Dictionary<String,String>
    //  cell.backgroundColor = UIColor.white
    
    cell.lbl_name.text = dict?["name"]
    cell.lbl_category.text = dict?["category_name"]
    // cell.image_view.image = UIImage.init(named: (dict?["primary_image"])!)
//    let imageString =  dict?["primary_image"]
//    
//    
//    
//    DispatchQueue.main.async {
//        let imgURL = URL(string:imageString!)
//        cell.image_view.kf.setImage(with: imgURL)}
    
    
    return cell
    
}
func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
{
    return 115
}
//func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
//{
//    
//    let dict = careerArr[indexPath.row] as? Dictionary<String,String>
//    let url = (dict?["landing_page"])! as String
//    let storyboard = UIStoryboard(name:"Main", bundle: nil)
//    let vc  = storyboard.instantiateViewController(withIdentifier:"CareerdetailViewController") as! CareerdetailViewController
//    self.navigationItem.title = ""
//    vc.detail_url = url
//    self.navigationController?.pushViewController(vc, animated: true)
//}



//func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//    if(indexPath.row == (self.careerArr.count-1)) {
//        // if isMoreDataAvailable {
//        paging = paging + 1
//        self.callgetCareerListMethod()
//        // }
//        
//        
//    }
//    
//    
//}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
