//
//  BusinessDevelopmentViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 3/21/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class BusinessDevelopmentViewController: UIViewController {

     @IBOutlet var btn_checklist: UIButton!
    @IBOutlet var btn_minibusinessPlan: UIButton!
      @IBOutlet var btn_resources: UIButton!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationItem.title = "BUSINESS DEVELOPMENT"
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        btn_checklist.layer.cornerRadius = btn_checklist.frame.size.height/2
         btn_minibusinessPlan.layer.cornerRadius = btn_minibusinessPlan.frame.size.height/2
       btn_resources.layer.cornerRadius = btn_resources.frame.size.height/2
    }
    
    
    @IBAction func btn_checkListAction(_ sender: Any) {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"BusinessCheckListViewController") as! BusinessCheckListViewController
         self.navigationItem.title = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func btn_miniBusinessPlanAction(_ sender: Any) {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"MiniBusinessPlanViewController") as! MiniBusinessPlanViewController
        self.navigationItem.title = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_resourcesAction(_ sender: Any) {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"ResourcesViewController") as! ResourcesViewController
        self.navigationItem.title = ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
