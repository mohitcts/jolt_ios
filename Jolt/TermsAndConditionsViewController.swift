//
//  TermsAndConditionsViewController.swift
//  Jolt
//
//  Created by chawtech solutions on 3/18/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class TermsAndConditionsViewController: UIViewController,UIWebViewDelegate {
    @IBOutlet weak var webview: UIWebView!
 var activityIndicatorView: ActivityIndicatorView!
    
        override func viewDidLoad() {
        super.viewDidLoad()
            webview.scrollView.bounces = false
            self.callTermsAndConditionsMethod()
        // Do any additional setup after loading the view.
    }
    @IBAction func btn_cancelAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK:-  server call of login method
    func callTermsAndConditionsMethod()
    {
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
            
        }
        let apiCall = JsonApi()
        let parameters = [
            "id":"1"
           
        ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionaltermsAndConditionsUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            let status = result["status"] as! String
            if status != ""
            {
                let success = result["success"] as! String
              
                
                print(status)
                if success == "true"
                {
                    // set all key values in user defaults
                    let data = result["data"] as! NSDictionary
                    let description = data["description"]
                   
                       
                    self.webview.loadHTMLString(description as! String, baseURL: nil)
                    }
                else
                {
                    // call alert controller in main queue
                    DispatchQueue.main.async {
                        self.activityIndicatorView.stopAnimating()
                        self.displayAlertWithMessage(viewController:self, title:"Alert!" , message:"Some Error happen! please try again")
                    }
                }
                
            }
            else
            {
                
            }
        }
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
         DispatchQueue.main.async {
         self.activityIndicatorView.stopAnimating()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
