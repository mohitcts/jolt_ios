//
//  RegistrationViewController.swift
//  Jolt
//
//  Created by ChawtechSolutions on 27/02/17.
//  Copyright © 2017 ChawtechSolutions. All rights reserved.
//

import UIKit

class RegistrationViewController: UIViewController {
    @IBOutlet weak var txtf_password: CustomTextField!
    @IBOutlet weak var txtf_email: CustomTextField!
    @IBOutlet weak var txtf_confirmpassword: CustomTextField!
    @IBOutlet weak var txtf_name: CustomTextField!
    var activityIndicatorView: ActivityIndicatorView!
     @IBOutlet weak var btn_checkBox: UIButton!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
         self.txtf_name.bs_setupErrorMessageView(withMessage: "Please Enter Name")
         self.txtf_confirmpassword.bs_setupErrorMessageView(withMessage: "Please Does Not Match")
        self.txtf_email.bs_setupErrorMessageView(withMessage: "Please Enter Email")
        self.txtf_password.bs_setupErrorMessageView(withMessage: "Please Enter Password")
        
        
        //TODO:-  set tap gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.removeKeyboard(_:)))
        self.view.addGestureRecognizer(tapGesture)
    }
    
    //MARK:- remove Keyboard action of tapgesture
    func removeKeyboard(_ sender: UITapGestureRecognizer) {
        
        self.view.endEditing(true)
    }
    @IBAction func btn_cancelAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: false)
    }
    
    //MARK:- btn checkbox Having tyre action
    @IBAction func btn_termsandConditions(sender: UIButton)
    {
        let storyboard = UIStoryboard(name:"Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier:"TermsAndConditionsViewController") as! TermsAndConditionsViewController
        self.present(vc, animated: true, completion: nil)
        //self.navigationController?.pushViewController(vc, animated: false)
    }
    
    
    //MARK:- btn checkbox Having tyre action
    @IBAction func btn_checkboxterms(sender: UIButton)
    {
        if (btn_checkBox.isSelected == true)
        {
            btn_checkBox.setBackgroundImage(UIImage(named:"unselected_checkbox"), for: UIControlState.normal)
            
            btn_checkBox.isSelected = false;
        }
        else
        {
            btn_checkBox.setBackgroundImage(UIImage(named:"selected_checkbox"), for: UIControlState.normal)
            
            btn_checkBox.isSelected = true;
        }
    }

    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        textField.bs_hideError()
    }
    //MARK:- btn signup Action
    @IBAction func btn_signupAction(_ sender: Any)
    {
        
        if ((txtf_name.text?.characters.count)! > 0)
        {
            if ((txtf_email.text?.characters.count)! > 0)
            {
                if  isValidEmail(testStr:txtf_email.text!)  == true
                {
                     if ((txtf_password.text?.characters.count)! > 0)
                     {
                        if  txtf_password.text == txtf_confirmpassword.text
                        {
                            if btn_checkBox.isSelected == true
                            {
                                self.callRegisterMethod()
                            }
                            else
                            {
                                self.displayAlertWithMessage(viewController:self, title: "Alert!", message: "Please Accept Terms And Conditions")
                            }
                        }
                        else
                        {
                            self.txtf_confirmpassword.bs_showError()
                        }
                     }
                     else
                     {
                         self.txtf_password.bs_showError()
                    }
                }
                else
                {
                    self.txtf_email.bs_setupErrorMessageView(withMessage: "Please Enter Valid  Email")
                    self.txtf_email.bs_showError()
                }
            }
            else
            {
                 self.txtf_email.bs_showError()
            }
            
        }else
        {
            self.txtf_name.bs_showError()
        }
        
        
        
        
        
        
//        
//        if ((txtf_email.text?.characters.count)! > 0)
//            && ((txtf_password.text?.characters.count)! > 0)
//            && ((txtf_confirmpassword.text?.characters.count)! > 0)
//            && ((txtf_name.text?.characters.count)! > 0)
//            
//            
//            
//        {
//            if  isValidEmail(testStr:txtf_email.text!)  == true
//            {
//                if  txtf_password.text == txtf_confirmpassword.text
//                {
//                    // call register method
//                    self.callRegisterMethod()
//                }
//                else
//                {
//                    DispatchQueue.main.async
//                        {
//                            self.displayAlertWithMessage(viewController: self, title:"Alert!", message:"Password and confirm password does not match")
//                    }
//                    
//                }
//                
//            }
//            else
//            {
//                DispatchQueue.main.async {
//                    self.displayAlertWithMessage(viewController: self, title:"Alert!", message:"Email is not in correct format")
//                }
//            }
//            
//        }
//        else
//        {
//            DispatchQueue.main.async {
//                self.displayAlertWithMessage(viewController: self, title:"Alert!", message:"Field can't be empty")
//            }
//            
//        }
    }
    
    //MARK:- check email format function
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    // MARK:- Server call of register Method
    func callRegisterMethod()
    {
        activityIndicatorView = ActivityIndicatorView(title: "Loading...", center: self.view.center)
        self.view.addSubview(activityIndicatorView.getViewActivityIndicator())
        DispatchQueue.main.async {
            self.activityIndicatorView.startAnimating()
        }
        
        let apiCall = JsonApi()
        let parameters = [
            
            "password": txtf_password.text!,
            "email": txtf_email.text!,
            "name": txtf_name.text!,
            "registration_type":"",
            "device": "ios",
            "fcm_key":UserDefaults.standard.object(forKey: "fcm_token")!
            
            
        ]
        
        apiCall.callUrlSession(urlValue: kbaseUrl + kadditionalRegisterUrl, para: parameters as (AnyObject), isSuccess: true)
        { (result) -> Void in
            
            print("service-",result)
            let status = result["status"] as! String
            if status != ""
            {
                let success = result["success"] as! String
                let message = result["message"] as! String
                
                print(status)
                if success == "true"
                {
                    // set all key values in user defaults
                    let data = result["data"] as! NSDictionary
                  
                    UserDefaults.standard.setValue(data["email"], forKey: "email")
                  
                  
                    UserDefaults.standard.setValue(data["name"], forKey: "name")
                  
                 
                    UserDefaults.standard.setValue(data["id"], forKey: "id")
                    UserDefaults.standard.setValue(data["registerDate"], forKey: "registerDate")
                    UserDefaults.standard.setValue(data["password"], forKey: "password")
                    
                    // call next view controller in main queue
                    DispatchQueue.main.async {
                        self.activityIndicatorView.stopAnimating()
                        let storyboard = UIStoryboard(name:"Main", bundle: nil)
                        let vc  = storyboard.instantiateViewController(withIdentifier: "TabBarViewController") as! TabBarViewController
                        vc.selectedIndex = 1
                        self.navigationController?.pushViewController(vc, animated: false)
                    }

                }
                else
                {
                    // call alert controller
                    DispatchQueue.main.async
                        {
                             self.activityIndicatorView.stopAnimating()
                            self.displayAlertWithMessage(viewController: self, title: "Alert!", message:message)
                    }
                }
                
            }
            else
            {
                // call alert controller
                DispatchQueue.main.async
                    {
                        self.activityIndicatorView.stopAnimating()
                        self.displayAlertWithMessage(viewController: self, title: "Alert!", message:"Server Error! Please try again")
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
